﻿using System.Xml.Linq;
using RendererInCSharp.bsdf;
using RendererInCSharp.core;
using RendererInCSharp.integrator;
using RendererInCSharp.material;
using RendererInCSharp.material.emit;
using RendererInCSharp.shape;
using RendererInCSharp.shape._2dShape;
using RendererInCSharp.shape._3dShape;
using RendererInCSharp.texture;

namespace RendererInCSharp.utils;

/// <summary>
/// Factory pattern. In order to create Objects with XML file.
/// </summary>
internal class Factory
{
    /// <summary>
    /// Load <see cref="Scene"/>> from an XML file.
    /// </summary>
    /// <param name="filename">XML filename</param>
    /// <returns>Scene</returns>
    /// <exception cref="InvalidDataException"></exception>
    public static Integrator LoadSceneFromXML(string filename)
    {
        var doc = XDocument.Load(filename);

        var scene = new Scene(CreateCameraFromXML(doc.Descendants("camera").First())); //Assume there is only one camera.
        
        var shapes = doc.Descendants("shape");
        foreach (var e in shapes)
        {
            var s = CreateShapeFromXML(e);
            if (s.Mat.Emitter != null) scene.RegLight(s); else scene.RegShape(s);
        }
        
        if (doc.Descendants("bType").First() is { } type) 
            scene.Build(type.Value); 
        else 
            throw new InvalidDataException("scene element missing.");
        
        return CreateIntegratorFromXML(doc.Descendants("integrator").First(), scene);
    }

    /// <summary>
    /// Create <see cref="Integrator"/> from XML Object.
    /// </summary>
    /// <param name="xml">XML Fragment</param>
    /// <param name="s"> Scene need to be register</param>
    /// <returns>Integrator ready to render</returns>
    /// <exception cref="InvalidDataException"></exception>
    public static Integrator CreateIntegratorFromXML(XElement xml, Scene s)
    {
        if (xml.Element("type") is { } type)
        {
            switch (type.Value)
            {
                case "PT":
                    return new PathTracing(s);
                case "NEE":
                    return new NEE(s);
            }
        }

        throw new InvalidDataException("integrator element missing.");
    }

    /// <summary>
    /// Create <see cref="Point"/> from XML Object.
    /// </summary>
    /// <param name="xml">XML Fragment</param>
    /// <returns>Point</returns>
    /// <exception cref="InvalidDataException"></exception>
    public static Point CreatePointFromXML(XElement xml)
    {
        if (xml.Element("x") is { } x &&
            xml.Element("y") is { } y &&
            xml.Element("z") is { } z)
        {
            return new Point(float.Parse(x.Value), float.Parse(y.Value), float.Parse(z.Value));
        }

        throw new InvalidDataException("vector element missing.");
    }

    /// <summary>
    /// Create <see cref="Camera"/> from XML Object.
    /// </summary>
    /// <param name="xml">XML Fragment</param>
    /// <returns></returns>
    /// <exception cref="InvalidDataException"></exception>
    public static Camera CreateCameraFromXML(XElement xml)
    {
        if (xml.Element("type") is { } type &&
            xml.Element("screenWidth") is { } w &&
            xml.Element("screenHeight") is { } h &&
            xml.Element("spp") is { } spp &&
            xml.Element("depth") is { } depth &&
            xml.Element("FOV") is { } fov &&
            xml.Element("defocusAngle") is { } defocus &&
            xml.Element("focusDistance") is { } focus &&
            xml.Element("origin") is { } o &&
            xml.Element("faceTo") is { } ft &&
            xml.Element("normal") is { } n)
        {
            switch (type.Value)
            {
                case "JP":
                    return new JitteredPerspectiveCamera(int.Parse(w.Value),
                                                         int.Parse(h.Value),
                                                         int.Parse(spp.Value),
                                                         int.Parse(depth.Value),
                                                         CreatePointFromXML(o),
                                                         CreatePointFromXML(ft),
                                                         CreatePointFromXML(n),
                                                         float.Parse(fov.Value),
                                                         float.Parse(defocus.Value),
                                                         float.Parse(focus.Value));
                case "NP":
                    return new RandomPerspectiveCamera(int.Parse(w.Value),
                                                       int.Parse(h.Value),
                                                       int.Parse(spp.Value), 
                                                       int.Parse(depth.Value), 
                                                       CreatePointFromXML(o),
                                                       CreatePointFromXML(ft),
                                                       CreatePointFromXML(n),
                                                       float.Parse(fov.Value),
                                                       float.Parse(defocus.Value),
                                                       float.Parse(focus.Value));
            }
        }

        throw new InvalidDataException("camera element missing.");
    }

    /// <summary>
    /// create <see cref="IShape"/> From XML Object.
    /// </summary>
    /// <param name="xml">XML Fragment</param>
    /// <returns></returns>
    /// <exception cref="InvalidDataException"></exception>
    public static IShape CreateShapeFromXML(XElement xml)
    {
        if (xml.Element("type") is { } type)
        {
            switch (type.Value)
            {
                case "Quad":
                {
                    if (xml.Element("origin") is { } o &&
                        xml.Element("u") is { } u &&
                        xml.Element("v") is { } v &&
                        xml.Element("material") is { } mat ) return new Quad(CreatePointFromXML(o), CreatePointFromXML(u), CreatePointFromXML(v), CreateMaterialFromXML(mat));
                    break;
                }
                case "Sphere":
                {
                    if (xml.Element("origin") is { } o &&
                        xml.Element("radius") is { } r &&
                        xml.Element("material") is { } mat) return new Sphere(CreatePointFromXML(o), float.Parse(r.Value), CreateMaterialFromXML(mat));
                    break;
                }
                case "Box":
                {
                    if (xml.Element("min") is { } min &&
                        xml.Element("max") is { } max &&
                        xml.Element("material") is { } mat) return Instance.Box(CreatePointFromXML(min), CreatePointFromXML(max), CreateMaterialFromXML(mat));
                    break;
                }
                case "OBJ":
                {
                    if (xml.Element("file") is { } file &&
                        xml.Element("material") is { } mat) return CreateShapeFromOBJ(file.Value, CreateMaterialFromXML(mat));
                    break;
                }
                //case "Homogeneity":
                //{
                //    if (xml.Element("shape") is { } s &&
                //        xml.Element("density") is { } den &&
                //        xml.Element("texture") is { } tex) return new Homogeneity(CreateShapeFromXML(s), float.Parse(den.Value), CreateTextureFromXML(tex));
                //    break;
                //}
            }
        }

        throw new InvalidDataException("shape element missing.");
    }

    /// <summary>
    /// OBJ reader
    /// </summary>
    /// <param name="objFile">obj filename</param>
    /// <param name="mat">pre-created material.</param>
    /// <returns><see cref="BVH"/> struct of all triangles</returns>
    /// <exception cref="InvalidDataException"></exception>
    public static IShape CreateShapeFromOBJ(string objFile, Material mat)
    {
        var points = new List<Point>();
        var ans = new List<IShape>();

        var sr = new StreamReader(objFile);

        var line = sr.ReadLine();
        while (line != null)
        {
            var data = line.Trim().Split(' ');

            switch (data[0])
            {
                case "#": 
                    break;

                case "v":
                {
                    if (data.Length is not (4 or 5)) throw new InvalidDataException("unknown obj format.");
                    points.Add(new Point(float.Parse(data[1]), float.Parse(data[2]), float.Parse(data[3])));
                    break;
                }

                case "f":
                {
                    if (data.Length is not 4) throw new InvalidDataException("unknown obj format.");
                    ans.Add(new Triangle(points[int.Parse(data[1]) - 1],
                                              points[int.Parse(data[2]) - 1] - points[int.Parse(data[1]) - 1],
                                              points[int.Parse(data[3]) - 1] - points[int.Parse(data[1]) - 1], mat));
                    break;
                }

                default:
                    throw new InvalidDataException("unknown obj format.");
            }

            line = sr.ReadLine();
        }

        sr.Close();
        var k = new BVH(ans);
        Trace.WriteLine($"OBJ read with {points.Count} points and {ans.Count} triangles. in {k.BBox}");
        return k;
    }

    /// <summary>
    /// Create <see cref="ILight"/>> from XML Object.
    /// </summary>
    /// <param name="xml">XML fragment</param>
    /// <returns></returns>
    /// <exception cref="InvalidDataException"></exception>
    public static ILight CreateLightFromXML(XElement xml)
    {
        if (xml.Element("type") is { } type)
        {
            switch (type.Value)
            {
                case "Point":
                {
                    if (xml.Element("color") is { } c) return new PointLight(CreatePointFromXML(c));
                    break;
                }
                case "Area":
                {
                    if (xml.Element("texture") is { } tex) return new AreaLight(CreateTextureFromXML(tex));
                    break;
                }
            }
        }

        throw new InvalidDataException("light element missing.");
    }

    /// <summary>
    /// create <see cref="BSDF"/> object from XML Object.
    /// </summary>
    /// <param name="xml">XML fragment</param>
    /// <returns></returns>
    /// <exception cref="InvalidDataException"></exception>
    public static BSDF CreateBSDFFromXML(XElement xml)
    {
        if (xml.Element("type") is { } type)
        {
            switch (type.Value)
            {
                case "Diffuse":
                {
                    if (xml.Element("texture") is { } tex) return new DiffuseBSDF(CreateTextureFromXML(tex));
                    break;
                }
                case "Specular":
                {
                    if (xml.Element("albedo") is { } c &&
                        xml.Element("fuzz") is { } fuzz) return new SpecularBSDF(CreatePointFromXML(c), float.Parse(fuzz.Value));
                    break;
                }
                case "Dielectric":
                {
                    if (xml.Element("albedo") is { } c &&
                        xml.Element("fuzz") is { } fuzz &&
                        xml.Element("eta") is { } eta) return new DielectricBSDF(CreatePointFromXML(c), float.Parse(eta.Value), float.Parse(fuzz.Value));
                    break;
                }
            }
        }

        throw new InvalidDataException("bsdf element missing.");
    }

    /// <summary>
    /// create <see cref="Material"/> from XML Object.
    /// </summary>
    /// <param name="xml">XML fragment</param>
    /// <returns></returns>
    /// <exception cref="InvalidDataException"></exception>
    public static Material CreateMaterialFromXML(XElement xml)
    {
        if (xml.Element("type") is { } type)
        {
            switch (type.Value)
            {
                case "BSDF":
                {
                    if (xml.Element("bsdf") is { } bsdf) return new Material(CreateBSDFFromXML(bsdf));
                    break;
                }

                case "Light":
                {
                    if (xml.Element("light") is { } light) return new Material(CreateLightFromXML(light));
                    break;
                }
            }
        }

        throw new InvalidDataException("material element missing.");
    }

    /// <summary>
    /// create <see cref="ITexture"/> from XML Object.
    /// </summary>
    /// <param name="xml">XML fragment</param>
    /// <returns></returns>
    /// <exception cref="InvalidDataException"></exception>
    public static ITexture CreateTextureFromXML(XElement xml)
    {
        if (xml.Element("type") is { } type)
        {
            switch (type.Value)
            {
                case "Solid":
                {
                    if (xml.Element("color") is { } c) return new SolidTex(CreatePointFromXML(c));
                    break;
                }
                case "Checkerboard":
                {
                    if (xml.Element("scale") is { } s &&
                        xml.Element("tex1") is { } t1 &&
                        xml.Element("tex2") is { } t2) return new CheckerboardTex(float.Parse(s.Value), CreateTextureFromXML(t1), CreateTextureFromXML(t2));
                    break;
                }
            }
        }

        throw new InvalidDataException("texture element missing.");
    }
}

