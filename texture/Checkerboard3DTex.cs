﻿namespace RendererInCSharp.texture;

internal class Checkerboard3DTex: ITexture
{
    /// <summary>
    /// checkerboard texture with the coordinate in 3D.
    /// </summary>
    /// <param name="scale">size of block</param>
    /// <param name="tex1">Texture in even</param>
    /// <param name="tex2">Texture in odd</param>
    public Checkerboard3DTex(float scale, ITexture tex1, ITexture tex2)
    {
        _scale = scale;
        _tex1 = tex1;
        _tex2 = tex2;
    }

    /// <summary>
    /// checkerboard texture with the coordinate in 3D.
    /// </summary>
    /// <param name="scale">size of block</param>
    /// <param name="c1">solid texture in even</param>
    /// <param name="c2">solid texture in odd</param>
    public Checkerboard3DTex(float scale, Color c1, Color c2)
    {
        _tex1 = new SolidTex(c1);
        _tex2 = new SolidTex(c2);
        _scale = scale;
    }

    public Color GetColor(float u, float v, Point p)
    {
        var a = (int)float.Floor(u / _scale);
        var b = (int)float.Floor(v / _scale / 2.0f);

        return (a + b) % 2 == 0 ? _tex1.GetColor(u, v, p) : _tex2.GetColor(u, v, p);
    }

    private readonly float _scale;
    private readonly ITexture _tex1, _tex2;
}
