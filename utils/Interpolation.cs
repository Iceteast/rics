﻿namespace RendererInCSharp.utils;

/// <summary>
/// Interpolation Functions
/// </summary>
internal class Interpolation
{
    /// <summary>
    /// Color Interpolation in 1D.
    /// </summary>
    /// <param name="u">parameter</param>
    /// <param name="a">Color 1</param>
    /// <param name="b">Color 2</param>
    /// <returns>new Color</returns>
    public static Color Interpolation1D(float u, Color a, Color b)
    {
        return a * u + b * (1 - u);
    }

    /// <summary>
    /// float Interpolation in 1D.
    /// </summary>
    /// <param name="u">parameter</param>
    /// <param name="a">float 1</param>
    /// <param name="b">float 2</param>
    /// <returns>new float</returns>
    public static float Interpolation1D(float u, float a, float b)
    {
        return a * u + b * (1 - u);
    }

    /// <summary>
    /// Color Interpolation in 3D.
    /// </summary>
    /// <param name="u">parameter in u axis</param>
    /// <param name="v">parameter in v axis</param>
    /// <param name="w">parameter in w axis</param>
    /// <param name="vertices">8 Colors</param>
    /// <returns>new Color</returns>
    public static float Interpolation3D(float u, float v, float w, ref Point[,,] vertices)
    {
        var uu = u * u * (3 - 2 * u);
        var vv = v * v * (3 - 2 * v);
        var ww = w * w * (3 - 2 * w);

        var ans = 0.0f;

        for (var i = 0; i < 2; i++)
        {
            for (var j = 0; j < 2; j++)
            {
                for (var k = 0; k < 2; k++)
                {
                    ans += Interpolation1D(i, uu, 1 - uu) * 
                           Interpolation1D(j, vv, 1 - vv) *
                           Interpolation1D(k, ww, 1 - ww) * 
                           Point.Dot(vertices[i, j, k], new Point(u - i, v - j, w - k));
                }
            }
        }
        return ans;
    }

    /// <summary>
    /// float Interpolation in 3D.
    /// </summary>
    /// <param name="u">parameter in u axis</param>
    /// <param name="v">parameter in v axis</param>
    /// <param name="w">parameter in w axis</param>
    /// <param name="vertices">8 float</param>
    /// <returns>new float</returns>
    public static float Interpolation3D(float u, float v, float w, float[,,] vertices)
    {
        var ans = 0.0f;
        for (var i = 0; i < 2; i++)
        {
            for (var j = 0; j < 2; j++)
            {
                for (var k = 0; k < 2; k++)
                {
                    ans += Interpolation1D(i, u, 1 - u) * 
                           Interpolation1D(j, v, 1 - v) *
                           Interpolation1D(k, w, 1 - w) * 
                           vertices[i, j, k];
                }
            }
        }
        return ans;
    }
}
