﻿using RendererInCSharp.material;
using RendererInCSharp.shape;
using RendererInCSharp.utils;

namespace RendererInCSharp.core
{
    public class Scene(Camera cam)
    {
        public void RegLight(IShape light)
        {
            _shapes.Add(light);
            Lights.Add(light);
        }

        public void RegShape(IShape shape)
        {
            _shapes.Add(shape);
        }

        public void Build(string type)
        {
            _builtScene = type switch
            {
                "BVH"   => new BVH(_shapes),
                "Naive" => new Instance(_shapes),
                _       => _builtScene
            };

            _shapes.Clear();
        }

        public (ShapeSample, ILight) SampleLight()
        {
            var shape = Lights[Utils.Generator.Next(Lights.Count)];

            if (shape.Mat.Emitter is { } light) return (shape.Sample(), light);
            
            throw new InvalidOperationException("Target is not a light source.");
        }

        public bool Hit(Ray r, Interval t, out HitRec rec)
        {
            rec = new HitRec();

            if (_builtScene is { } ins) return ins.Hit(r, t, ref rec);

            throw new InvalidOperationException("scene isn't built.");
        }

        public readonly Camera Cam = cam;
        public readonly List<IShape> Lights = [];
        private readonly List<IShape> _shapes = [];
        private IShape? _builtScene;
    }
}
