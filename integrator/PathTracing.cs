﻿using RendererInCSharp.core;
using RendererInCSharp.shape;
using RendererInCSharp.utils;

namespace RendererInCSharp.integrator
{
    internal class PathTracing(Scene scene) : Integrator(scene)
    {
        protected override Color GetColor(Ray r, int depth)
        {
            var ans = Color.Zero;
            var acc = Color.One;

            for (var i = 0; i < depth; i++)
            {
                if (!Scene.Hit(r, new Interval(0.0001f, float.MaxValue), out var rec)) break;

                if (rec.Mat.Emitter != null && rec.FrontFace) return rec.Mat.Emitter.Emission(-r.Direction, rec).Intensity * acc;

                var sp = Utils.RussianRoulette(acc, 0.75f);
                if (Utils.Generator.NextSingle() > sp) break;

                if (rec.Mat.Bsdf is not { } bsdf) break;

                var s = bsdf.Sample(rec, r.Direction);    // get the next sample.
                s.PDF *= sp;                                                // add the RR probability

                // update acc with hit cosine and color.
                acc *= s.Intensity * float.Abs(Vector.Dot(Vector.Normalize(s.Direction), rec.Normal)) / s.PDF;
                r = new Ray(rec.HitPoint, s.Direction, r.Time);                       // update ray
            }

            return ans;
        }
    }
}
