﻿using RendererInCSharp.utils;
using RendererInCSharp.material;

namespace RendererInCSharp.shape._2dShape
{
    internal class Quad: Plane
    {
        public Quad(Point p, Vector u, Vector v, Material mat) : base(p, u, v, mat)
        {
            BBox = new AABB(p, p + u + v) + new AABB(p + u, p + v);
        }

        protected override bool IsInterior(float a, float b, ref HitRec rec)
        {
            var k = new Interval(0, 1);

            if (!k.Contains(a) || !k.Contains(b)) return false;

            rec.U = a;
            rec.V = b;
            return true;
        }

        public override float GetArea()
        {
            return Vector.Cross(U, V).Length();
        }

        public override string ToString()
        {
            return "Quad";
        }
    }
}
