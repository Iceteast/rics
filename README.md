﻿# Rerderer in CSharp

## TODO:

- Quadic
- ~~Triangle~~
- ~~BVH toooooooooo ugly! rebuild~~
- ~~rebuild Image class | png file output.~~
- ~~make all Random using a global seed variable.~~
- ~~BBox might have some issues.~~
- ~~rebuild V3 by Vector3.~~
- Matrix expr
- ~~ToString Formating~~
- ~~Scene rebuild~~
- ~~Rotation on XYZ~~
- ~~XML for Scene, add Factory class.~~
	- ~~reshape Scene conductor to xml~~
	- add schema and shortcut for materials usw.
- 
- ~~Parallel.For~~
- 
- Mesh class
- homo material (hard to do).
- ~~rebuild material as abstract class ???~~
- 
- correctly finished PT and NEE.
- MIS or Volumetric?
- 
- add summary for all codes.
- ~~1d texture~~
- enviroment map
- ~~OBJ reader.~~
- 

## Need to learn

- LINQ(partly finished)
- 

## Possible Risk

- ~~Rotation Normal~~
- ~~AreaLight Emission~~
- ~~Some Shape can't be moved or suddenly disappeared.~~

## Question

- Is it possible to add a interface property in interface?
- ~~Constructor reuse~~
- is it necessary to use different generator for each pixel?
- ~~how to attach an example XML file while building?~~

## Run

main function in `RiCS.cs`. 
Scene filename and output filename are hardcode in `RiCS.cs`.

## Meeting

1. Basic Info

   - 简单的perspective camera, 简单的shapes, 简单的BVH, 简单的Texture和BSDF.
   - OBJreader, XMLparser.
   - FactoryFunction
2. Plan for now
  - not yet Implement
    - quadic shapes / mesh class
    - volumetric rendering
    - MIS
    - enviroment map
    - homomorphism material
  - has problem
    - dieletric material
    - pathtracing Illumination.
3. Question if all goes well
  - Why using different generator for each pixel in SeeSharp?
  - Why some renderer treat Light as material, some are not?
4. About topic
   - I don't know what leads to this difficult situation. Is this 