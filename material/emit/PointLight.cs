﻿using RendererInCSharp.sampler;
using RendererInCSharp.shape;
using RendererInCSharp.utils;

namespace RendererInCSharp.material.emit
{
    internal class PointLight(Color intensity) : ILight
    {
        public DirectSample SampleDirection(ShapeSample sample, Vector from)
        {
            return new DirectSample
            {
                Position = sample.SamplePoint,
                Intensity = _intensity,
                PDFArea = 1.0f,
                PDFDirection = PDFSampler.UniformSpherePDF(),
                Cosine = 1.0f
            };
        }

        public EmissionSample SampleEmission(ShapeSample sample)
        {
            var pdfSample = PDFSampler.UniformSphereSample();

            return new EmissionSample
            {
                Position = sample.SamplePoint,
                Direction = pdfSample.Direction,
                Intensity = _intensity,
                PDFArea = 1.0f,
                PDFDirection = pdfSample.PDF,
                Cosine = 1.0f
            };
        }

        public EmissionValue Emission(Vector direction, HitRec rec)
        {
            return new EmissionValue
            {
                Intensity = Color.Zero, 
                //PDFArea = 1.0f,
                PDFDirection = 1.0f
            };
        }

        public bool HasArea()
        {
            return false;
        }

        private readonly Color _intensity = intensity / (4.0f * Utils.PI);
    }
}
