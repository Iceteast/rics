﻿namespace RendererInCSharp.utils;

internal class Fresnel
{
    /// <summary>
    /// Calculate Fresnel coefficient.
    /// </summary>
    /// <param name="eta">Transmission coefficient</param>
    /// <param name="cosI">cosine of in-coming ray</param>
    /// <param name="cosT">cosine of out-coming ray</param>
    /// <returns>Fresnel coefficient</returns>
    public static float Factor(float eta, float cosI, float cosT)
    {
        var rs = (eta * cosI - cosT) / (eta * cosI + cosT);
        var rp = (cosI - eta * cosT) / (cosI + eta * cosT);
        return (rs * rs + rp * rp) * 0.5f;
    }
}

