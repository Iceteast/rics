﻿using RendererInCSharp.core;
using RendererInCSharp.material;
using RendererInCSharp.utils;

namespace RendererInCSharp.shape._2dShape
{
    internal class Plane : IShape
    {
        public Plane(Point p, Vector u, Vector v, Material mat)
        {
            Origin = p;
            U = u;
            V = v;
            Mat = mat;
            var q = p + u + v;

            BBox = new AABB(double.Abs(q.X) < 10e-4 ? Interval.EMPTY : Interval.FULL,
                            double.Abs(q.Y) < 10e-4 ? Interval.EMPTY : Interval.FULL,
                            double.Abs(q.Z) < 10e-4 ? Interval.EMPTY : Interval.FULL);

            var n = Vector.Cross(u, v);
            W = n / Vector.Dot(n, n);

            Normal = Vector.Normalize(n);
            D = Vector.Dot(Normal, p);
        }
        public bool Hit(Ray r, Interval t, ref HitRec rec)
        {
            var cosA = Vector.Dot(Normal, r.Direction);

            if (double.Abs(cosA) < 1e-8) return false;

            var time = (D - Vector.Dot(Normal, r.Origin)) / cosA;

            if (!t.Contains(time)) return false;

            var planeHitPoint = r.At(time) - Origin;
            var alpha = Vector.Dot(W, Vector.Cross(planeHitPoint, V));
            var beta = Vector.Dot(W, Vector.Cross(U, planeHitPoint));
            //System.Diagnostics.Trace.WriteLineIf(alpha < -0.1, alpha);
            if (!IsInterior(alpha, beta, ref rec)) return false;

            rec.T = time;
            rec.HitPoint = r.At(time - Utils.EPSILON);
            rec.Mat = Mat;
            rec.SetFaceNormal(r, Normal);

            return true;
        }

        public virtual ShapeSample Sample() //TODO : !!!!! Should be wrong in Plane.
        {
            var (a, b) = (Utils.Generator.NextSingle(), Utils.Generator.NextSingle());
            var rec = new HitRec();

            while (!IsInterior(a, b, ref rec)) 
            {
                (a, b) = (Utils.Generator.NextSingle(), Utils.Generator.NextSingle());
            }

            return new ShapeSample()
            {
                Normal = Normal,
                PDFArea = 1.0f / GetArea(),
                SamplePoint = U * a + V * b
            };
        }

        public virtual float GetArea()
        {
            return float.PositiveInfinity;
        }

        

        protected virtual bool IsInterior(float a, float b, ref HitRec rec)
        {
            return true;
        }

        public virtual ILight? GetLight()
        {
            return Mat.Emitter;
        }

        public override string ToString()
        {
            return "Plane";
        }

        protected readonly Point Origin;
        protected readonly Vector U, V, W, Normal;
        protected readonly float D;
        public AABB BBox { get; set; }
        public Material Mat { get; set; }
    }
}
