﻿using RendererInCSharp.texture.noise;

namespace RendererInCSharp.texture;

/// <summary>
/// Noise Texture.
/// </summary>
/// <param name="noise">Noise Generator</param>
/// <param name="scale">size of block</param>
internal class NoiseTex(INoise noise, float scale) : ITexture
{
    public Color GetColor(float u, float v, Point p)
    {
        return new Color(0.5f) * (1 + float.Sin(scale * p.Z + scale * p.Y * 0.5f + 10 * noise.GetIntensity(p)));
    }
}
