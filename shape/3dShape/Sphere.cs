﻿using RendererInCSharp.core;
using RendererInCSharp.material;
using RendererInCSharp.utils;

namespace RendererInCSharp.shape._3dShape
{
    internal class Sphere : IShape
    {
        public Sphere(Point o, float radius, Material mat)
        {
            Origin = o;
            _moveVector = new Point(0.0f);
            _radius = radius;
            Mat = mat;
            _isMoving = false;
            Shapes = new List<IShape>
            {
                this
            };

            var r = new Point(radius);
            BBox = new AABB(o - r, o + r);
        }

        public Sphere(Point o, Point o2, float radius, Material mat)
        {
            Origin = o;
            _moveVector = o2 - o;
            _radius = radius;
            Mat = mat;
            _isMoving = true;
            Shapes = new List<IShape>
            {
                this
            };

            var r = new Point(radius);
            BBox = new AABB(o - r, o + r) + new AABB(o2 - r, o2 + r);
        }

        private Point CurrentCenter(float tm)
        {
            return Origin + tm * _moveVector;
        }
        public bool Hit(Ray r, Interval t, ref HitRec rec)
        {
            var oc = (_isMoving ? CurrentCenter(r.Time) : Origin) - r.Origin;
            var a  = Point.Dot(r.Direction, r.Direction);
            var h  = Point.Dot(r.Direction, oc); // half b;
            var c  = oc.LengthSquared() - _radius * _radius;
            var d  = h * h - a * c;

            if (d < 0)
            {
                return false;
            }
            var sqrtD = float.Sqrt(d);
            var root = (h - sqrtD) / a;
            if (!t.Surrounds(root))
            {
                root = (h + sqrtD) / a;
                if (!t.Surrounds(root))
                {
                    return false;
                }
            }

            rec.T = root;
            rec.HitPoint = r.At(root);
            rec.Mat = Mat;

            var n = Vector.Normalize(rec.HitPoint - Origin);
            rec.SetFaceNormal(r, n);
            GetUV(n, out rec.U, out rec.V);

            return true;
        }

        public ShapeSample Sample()
        {
            var a = Utils.Generator.NextSingle();
            var b = Utils.Generator.NextSingle();
            var p = new Point(_radius  * float.Cos(a) * float.Sin(b), _radius * float.Sin(a) * float.Sin(b), _radius * float.Cos(b));
            return new ShapeSample
            {
                Normal = Vector.Normalize(p - Origin),
                PDFArea = 1.0f / GetArea(),
                SamplePoint = p
            };
        }

        public float GetArea()
        {
            return 4.0f * Utils.PI * _radius * _radius;
        }

        

        private static void GetUV(Point hitPoint, out float u, out float v)
        {
            var theta = float.Acos(-hitPoint.Y);
            var phi = float.Atan2(-hitPoint.Z, hitPoint.X) + Utils.PI;

            u = phi / (2.0f * Utils.PI);
            v = theta / Utils.PI;
        }

        public readonly Point Origin;

        private readonly Point _moveVector;
        private readonly float _radius;
        private readonly bool _isMoving;

        public AABB BBox { get; set; }
        public Material Mat { get; set; }
        public List<IShape> Shapes { get; set; }
    }
}
