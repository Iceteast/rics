﻿using RendererInCSharp.core;
using RendererInCSharp.material;
using RendererInCSharp.shape._2dShape;
using RendererInCSharp.utils;

namespace RendererInCSharp.shape
{
    /// <summary>
    /// A set of Shapes.
    /// </summary>
    public class Instance : IShape // TODO can be deleted. Only used in Box.
    {
        public Instance()
        {
            Mat = Material.NullMaterial; //TODO Might be wrong.
            BBox = new AABB();
            Shapes = [];
        }

        public Instance(List<IShape> shapes)
        {
            Mat = Material.NullMaterial; //TODO Might be wrong.
            BBox = new AABB();
            Shapes = [];

            foreach (var shape in shapes)
            {
                AddShape(shape);
            }
        }

        public void AddShape(IShape shape)
        {
            Shapes.Add(shape);
            BBox += shape.BBox;
            _area += shape.GetArea();
        }

        public bool Hit(Ray r, Interval t, ref HitRec rec)
        {
            var ans = false;
            var tmpRec = new HitRec();

            foreach (var _ in Shapes.Where(e => e.Hit(r, t, ref tmpRec)))
            {
                ans = true;
                t.Max = tmpRec.T;
                rec = tmpRec;
            }
            return ans;
        }

        public ShapeSample Sample() //TODO how it works? 
        {
            throw new NotImplementedException();
        }

        public float GetArea()
        {
            if (_area < Utils.EPSILON)
            {
                _area = 0.0f;
                foreach (var s in Shapes)
                {
                    _area += s.GetArea();
                }
            }

            return _area;
        }

        public List<IShape> Shapes { get; set; }
        public AABB BBox { get; set; }
        public Material Mat { get; set; }

        private float _area;

        /// <summary>
        /// A pre-created Box.
        /// </summary>
        /// <param name="a">left-bottom corner.</param>
        /// <param name="b">right-top corner.</param>
        /// <param name="mat">Material</param>
        /// <returns></returns>
        public static Instance Box(Point a, Point b, Material mat)
        {
            var ans = new Instance();

            var min = Vector.Min(a, b);
            var max = Vector.Max(a, b);

            var dx = new Vector(max.X - min.X, 0, 0);
            var dy = new Vector(-0, max.Y - min.Y, 0);
            var dz = new Vector(0, 0, max.Z - min.Z);

            ans.AddShape(new Quad(new Point(min.X, min.Y, max.Z),  dx,  dy, mat));
            ans.AddShape(new Quad(new Point(max.X, min.Y, max.Z), -dz,  dy, mat));
            ans.AddShape(new Quad(new Point(max.X, min.Y, min.Z), -dx,  dy, mat));
            ans.AddShape(new Quad(new Point(min.X, min.Y, min.Z),  dz,  dy, mat));
            ans.AddShape(new Quad(new Point(min.X, max.Y, max.Z),  dx, -dz, mat));
            ans.AddShape(new Quad(new Point(min.X, min.Y, min.Z),  dx,  dz, mat));
            ans.BBox = new AABB(min, max);
            return ans;
        }
    }
}
