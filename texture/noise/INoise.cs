﻿namespace RendererInCSharp.texture.noise;

/// <summary>
/// Interface of Noise.
/// </summary>
internal interface INoise
{
    /// <summary>
    /// return the value of noise.
    /// </summary>
    /// <param name="p">coordinate in 3D</param>
    /// <returns>noise value</returns>
    public float GetIntensity(Point p);
}

