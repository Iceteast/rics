﻿using RendererInCSharp.shape;

namespace RendererInCSharp.material
{
    public struct EmissionSample
    {
        public Color Intensity;
        public Vector Position;
        public Vector Direction;
        public float PDFArea;
        public float PDFDirection;
        public float Cosine;
    }

    public struct DirectSample
    {
        public Vector Position;
        public Color Intensity;
        public float PDFArea;
        public float PDFDirection;
        public float Cosine;
    }

    public struct EmissionValue
    {
        public Color Intensity;
        //public float PDFArea;
        public float PDFDirection;
    }
    public interface ILight
    {
        public DirectSample SampleDirection(ShapeSample sample, Vector from);
        public EmissionSample SampleEmission(ShapeSample sample);
        public EmissionValue Emission(Vector direction, HitRec rec);
        public bool HasArea();
    }
}
