﻿using RendererInCSharp.material;
using RendererInCSharp.utils;

namespace RendererInCSharp.shape._2dShape
{
    internal class Disk : Plane
    {
        public Disk(Point p, Vector u, Vector v, float r, Material mat) : base(p, u, v, mat)
        {
            _r = r;
            BBox = new AABB(p + u * r, p + v * r) + new AABB(p - u * r, p - v * r);
        }

        protected override bool IsInterior(float a, float b, ref HitRec rec)
        {
            if (a * a + b * b >= _r * _r) return false;

            rec.U = a;
            rec.V = b;
            return true;
        }

        public override float GetArea()
        {
            return Utils.PI * _r * _r;
        }

        public override string ToString()
        {
            return "Disk";
        }

        private readonly float _r;
    }
}
