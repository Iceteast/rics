﻿using RendererInCSharp.sampler;
using RendererInCSharp.utils;

namespace RendererInCSharp.core
{
    public abstract class Camera
    {
        public abstract List<Ray> GetRays(int x, int y);

        public int H, W, Spp, Depth;
    }

    public abstract class PerspectiveCamera : Camera
    {
        protected PerspectiveCamera(int imageWidth,
                                    int imageHeight,
                                    int spp,
                                    int depth,
                                    Vector origin,
                                    Vector faceTo,
                                    Vector normal,
                                    float vFov,
                                    float defocusAngle,
                                    float focusDistance)
        {
            H = imageHeight;
            W = imageWidth;
            Spp = spp;
            Depth = depth;

            CameraCenter = origin;
            DefocusAngle = defocusAngle;

            var direction = origin - faceTo;
            var ratio = (float) imageWidth / imageHeight;

            var scaleV = 2.0f * float.Tan(Utils.DegreeToRadian(vFov / 2.0f)) * focusDistance;
            var scaleU = scaleV * ratio;

            var screenU = Vector.Normalize(Vector.Cross(normal, direction)) * scaleU; //right-hand coordinate.
            var screenV = Vector.Normalize(-Vector.Cross(direction, screenU)) * scaleV;

            StepU = screenU / imageWidth;
            StepV = screenV / imageHeight;

            // Calculate the location of the upper left pixel.
            var viewportUpperLeft = CameraCenter - Vector.Normalize(direction) * focusDistance - screenU * 0.5f - screenV * 0.5f;
            Pixel00 = viewportUpperLeft +(StepU + StepV) * 0.5f;

            // Calculate the camera defocus disk basis vectors.
            var defocusRadius = focusDistance * float.Tan(Utils.DegreeToRadian(defocusAngle / 2.0f));

            DefocusU = Vector.Normalize(screenU) * defocusRadius;
            DefocusV = Vector.Normalize(screenV) * defocusRadius;
        }

        public override string ToString()
        {
            return $"[PerspectiveCamera] @ {CameraCenter}";
        }

        protected readonly Vector CameraCenter;
        protected readonly Vector Pixel00;
        protected readonly Vector StepU, StepV, DefocusU, DefocusV;
        protected readonly double DefocusAngle;
    }

    public class RandomPerspectiveCamera(int imageWidth,
                                   int imageHeight,
                                   int spp,
                                   int depth,
                                   Vector origin,
                                   Vector faceTo,
                                   Vector normal,
                                   float vFov,
                                   float defocusAngle,
                                   float focusDistance) : PerspectiveCamera(imageWidth,
                                    imageHeight,
                                    spp,
                                    depth,
                                    origin,
                                    faceTo,
                                    normal,
                                    vFov,
                                    defocusAngle,
                                    focusDistance)
    {
        public override List<Ray> GetRays(int x, int y)
        {
            var ans = new List<Ray>();

            for (var _ = 0; _ < Spp; _++)
            {
                var sD = Sampler.DiskSample2D();
                var sS = Sampler.UniformSample2D(-0.5f, 0.5f);

                var pointOnScreen =  Pixel00 + StepU * (x + sS.X) + StepV * (y + sS.Y);
                var pointOnLens = DefocusAngle <= 0 ? CameraCenter : CameraCenter + DefocusU * sD.X + DefocusV * sD.Y;

                var tm = Utils.Generator.NextSingle();

                ans.Add(new Ray(pointOnLens, Vector.Normalize(pointOnScreen - pointOnLens), tm));
            }
            
            return ans;
        }
    }

    public class JitteredPerspectiveCamera(int imageWidth,
                                           int imageHeight, 
                                           int spp,
                                           int depth,
                                           Color origin,
                                           Color faceTo,
                                           Color normal,
                                           float vFov,
                                           float defocusAngle,
                                           float focusDistance) : PerspectiveCamera(imageWidth,
                                                                                    imageHeight,
                                                                                    spp,
                                                                                    depth,
                                                                                    origin,
                                                                                    faceTo,
                                                                                    normal,
                                                                                    vFov,
                                                                                    defocusAngle,
                                                                                    focusDistance)
    {
        public override List<Ray> GetRays(int x, int y)
        {
            var ans = new List<Ray>();
            var sppSqrt = float.Ceiling(float.Sqrt(Spp));
            for (var sI = 0; sI < sppSqrt; sI++)
            {
                for (var sJ = 0; sJ < sppSqrt; sJ++)
                {
                    var sD = Sampler.DiskSample2D();
                    var sS = Sampler.StratifiedSquareSample2D(sI, sJ, 1.0f / sppSqrt);

                    var pointOnScreen =  Pixel00 + StepU * (x + sS.X) + StepV * (y + sS.Y);
                    var pointOnLens = DefocusAngle <= 0 ? CameraCenter : CameraCenter + DefocusU * sD.X + DefocusV * sD.Y;

                    var tm = Utils.Generator.NextSingle();

                    ans.Add(new Ray(pointOnLens, Vector.Normalize(pointOnScreen - pointOnLens), tm));
                }
            }
            
            return ans;
        }
    }
}
