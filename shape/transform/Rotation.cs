﻿using RendererInCSharp.core;
using RendererInCSharp.material;
using RendererInCSharp.utils;

namespace RendererInCSharp.shape.transform;

internal class Rotation : IShape
{
    /// <summary>
    /// Rotation around axis.
    /// </summary>
    /// <param name="shapes">origin shape</param>
    /// <param name="angle">rotate degree on each axis</param>
    public Rotation(IShape shapes, Vector angle)
    {
        Mat = shapes.Mat;
        _shapes = shapes;
        _radian = Utils.DegreeToRadian(angle);

        BBox = shapes.BBox;

        var min = new Vector(float.PositiveInfinity);
        var max = new Vector(float.NegativeInfinity);

        for (var i = 0; i < 2; i++)
        {
            for (var j = 0; j < 2; j++)
            {
                for (var k = 0; k < 2; k++)
                {
                    var vertex = new Vector(i * BBox.AxisX.Max + (1 - i) * BBox.AxisX.Min,
                                            j * BBox.AxisY.Max + (1 - j) * BBox.AxisY.Min,
                                            k * BBox.AxisZ.Max + (1 - k) * BBox.AxisZ.Min);

                    Rotate(_radian, ref vertex);

                    for (var l = 0; l < 3; l++)
                    {
                        min[l] = float.Min(min[l], vertex[l]);
                        max[l] = float.Max(max[l], vertex[l]);
                    }
                }
            }
        }
        BBox = new AABB(min, max);
    }

    public bool Hit(Ray r, Interval t, ref HitRec rec)
    {
        var rotatedRay = new Ray(r.Origin.Copy(), r.Direction.Copy(), r.Time);

        Rotate(-_radian, ref rotatedRay.Origin);
        Rotate(-_radian, ref rotatedRay.Direction);

        if (!_shapes.Hit(rotatedRay, t, ref rec)) return false;

        Rotate(_radian, ref rec.HitPoint);
        Rotate(_radian, ref rec.Normal);
        return true;
    }

    public ShapeSample Sample()
    {
        var sample = _shapes.Sample();
        Rotate(_radian, ref sample.Normal);
        Rotate(_radian, ref sample.SamplePoint);
        return sample;
    }

    public float GetArea()
    {
        return _shapes.GetArea();
    }

    public static void Rotate(Vector radian, ref Vector v)
    {
        RotateX(radian.X, ref v);
        RotateY(radian.Y, ref v);
        RotateZ(radian.Z, ref v);
    }

    private static void RotateX(float radian, ref Vector v)
    {
        (v.Y, v.Z) = (float.Cos(radian) * v.Y + float.Sin(radian) * v.Z, -float.Sin(radian) * v.Y + float.Cos(radian) * v.Z);
    }
    private static void RotateY(float radian, ref Vector v)
    {
        (v.X, v.Z) = (float.Cos(radian) * v.X + float.Sin(radian) * v.Z, -float.Sin(radian) * v.X + float.Cos(radian) * v.Z);
    }
    private static void RotateZ(float radian, ref Vector v)
    {
        (v.X, v.Y) = (float.Cos(radian) * v.X + float.Sin(radian) * v.Y, -float.Sin(radian) * v.X + float.Cos(radian) * v.Y);
    }

    private readonly IShape _shapes;
    private readonly Vector _radian;
    public AABB BBox { get; set; }
    public Material Mat { get; set; }
}
