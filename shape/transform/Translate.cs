﻿using RendererInCSharp.core;
using RendererInCSharp.material;
using RendererInCSharp.utils;

namespace RendererInCSharp.shape.transform;

/// <summary>
/// Translation shape.
/// </summary>
/// <param name="shapes">origin shape</param>
/// <param name="offset">offset in 3D</param>
internal class Translate(IShape shapes, Vector offset) : IShape
{
    public bool Hit(Ray r, Interval t, ref HitRec rec)
    {
        var rOffset = new Ray(r.Origin - offset, r.Direction, r.Time);
        if (!shapes.Hit(rOffset, t, ref rec)) return false;

        rec.HitPoint += offset;

        return true;
    }

    public ShapeSample Sample()
    {
        var sample = shapes.Sample();
        sample.SamplePoint += offset;
        return sample;
    }

    public float GetArea()
    {
        return shapes.GetArea();
    }


    public AABB BBox { get; set; } = shapes.BBox.Move(offset);
    public Material Mat { get; set; } = shapes.Mat;
}
