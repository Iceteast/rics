﻿//private void Scene2()
//{
//    var lambertianG = new Lambertian(new Color(0.8, 0.8, 0.0));
//    var lambertianC = new Lambertian(new Color(0.1, 0.2, 0.5));
//    var metalL = new Metal(new Color(0.8, 0.8, 0.8), 0.3);
//    var metalR = new Metal(new Color(0.8, 0.6, 0.2), 1.0);

//    ShapeSet.AddShape(new Sphere(new Point(0.0, -100.5, -1.0), 100.0, lambertianG));
//    ShapeSet.AddShape(new Sphere(new Point(0.0, 0.0, -1.2), 0.5, lambertianC));
//    ShapeSet.AddShape(new Sphere(new Point(-1.0, 0.0, -1.0), 0.5, metalL));
//    ShapeSet.AddShape(new Sphere(new Point(1.0, 0.0, -1.0), 0.5, metalR));
//}
//private void Scene3()
//{
//    _cam = new PerspectiveCamera(W, H, new ExpendVector(-2, 2, 1), new ExpendVector(0, 0, -1), new ExpendVector(0, 1, 0), 20, 0.1, 2.8);

//    var lambertianG = new Lambertian(new Color(0.8, 0.8, 0.0));
//    var lambertianC = new Lambertian(new Color(0.1, 0.2, 0.5));
//    var glassL = new Glass(new Color(1.0), 1.50, 0);
//    var metalR = new Metal(new Color(0.8, 0.6, 0.2), 1.0);

//    ShapeSet.AddShape(new Sphere(new Point(0.0, -100.5, -1.0), 100.0, lambertianG));
//    ShapeSet.AddShape(new Sphere(new Point(0.0, 0.0, -1.2), 0.5, lambertianC));
//    ShapeSet.AddShape(new Sphere(new Point(-1.0, 0.0, -1.0), 0.5, glassL));
//    ShapeSet.AddShape(new Sphere(new Point(1.0, 0.0, -1.0), 0.5, metalR));
//}
//private void Scene4()
//{
//    var lambertianG = new Lambertian(new Color(0.8, 0.8, 0.0));
//    var lambertianC = new Lambertian(new Color(0.1, 0.2, 0.5));
//    var glassL = new Glass(new Color(1.0), 1.50, 0.0);
//    var glassB = new Glass(new Color(1.0), 1 / 1.50, 0);
//    var metalR = new Metal(new Color(0.8, 0.6, 0.2), 1.0);

//    ShapeSet.AddShape(new Sphere(new Point(0.0, -100.5, -1.0), 100.0, lambertianG));
//    ShapeSet.AddShape(new Sphere(new Point(0.0, 0.0, -1.2), 0.5, lambertianC));
//    ShapeSet.AddShape(new Sphere(new Point(-1.0, 0.0, -1.0), 0.5, glassL));
//    ShapeSet.AddShape(new Sphere(new Point(-1.0, 0.0, -1.0), 0.4, glassB));
//    ShapeSet.AddShape(new Sphere(new Point(1.0, 0.0, -1.0), 0.5, metalR));
//}
//private void Scene5()
//{
//    _cam = new PerspectiveCamera(W, H, new(-2, 2, 1), new(0, 0, -1), new(0, 1, 0), 20, 0.6, 2.3);

//    var lambertianG = new Lambertian(new Color(0.8, 0.8, 0.0));
//    var lambertianC = new Lambertian(new Color(0.1, 0.2, 0.5));
//    var glassL = new Glass(new Color(1.0), 1.50, 0.0);
//    var glassB = new Glass(new Color(1.0), 1 / 1.50, 0);
//    var metalR = new Metal(new Color(0.8, 0.6, 0.2), 1.0);

//    ShapeSet.AddShape(new Sphere(new Point(0.0, -100.5, -1.0), 100.0, lambertianG));
//    ShapeSet.AddShape(new Sphere(new Point(0.0, 0.0, -1.2), 0.5, lambertianC));
//    ShapeSet.AddShape(new Sphere(new Point(-1.0, 0.0, -1.0), 0.5, glassL));
//    ShapeSet.AddShape(new Sphere(new Point(-1.0, 0.0, -1.0), 0.4, glassB));
//    ShapeSet.AddShape(new Sphere(new Point(1.0, 0.0, -1.0), 0.5, metalR));
//}
//private void SceneFinal()
//{
//    var lambertianG = new Lambertian(new Color(0.5));
//    var glass = new Glass(ExpendVector.WHITE, 1.50, 0);
//    var lambertianB = new Lambertian(new Color(0.4, 0.2, 0.1));
//    var meta = new Metal(new Color(0.7, 0.6, 0.5), 0.0);

//    ShapeSet.AddShape(new Sphere(new Point(0, -1000, 0), 1000, lambertianG));

//    var rom = new Random(1337);
//    var stp = new ExpendVector(4, 0.2, 0);

//    for (var i = -11; i < 11; i++)
//    {
//        for (var j = -11; j < 11; j++)
//        {
//            var flag = rom.NextDouble();
//            var c = new ExpendVector(i + 0.9 * rom.NextDouble(), 0.2, j + 0.9 * rom.NextDouble());

//            if ((c - stp).Length() <= 0.9) continue;

//            Material mat = flag switch
//            {
//                < 0.80 => new Lambertian(new ExpendVector(rom.NextDouble(), rom.NextDouble(), rom.NextDouble()) *
//                                           new ExpendVector(rom.NextDouble(), rom.NextDouble(), rom.NextDouble())),
//                < 0.95 => new Metal(new ExpendVector(rom.NextDouble() / 2 + 0.5, rom.NextDouble() / 2 + 0.5, rom.NextDouble() / 2 + 0.5), rom.NextDouble() * 0.5),
//                _ => glass
//            };
//            ShapeSet.AddShape(new Sphere(c, 0.2, mat));
//        }
//    }


//    ShapeSet.AddShape(new Sphere(new ExpendVector(0, 1, 0), 1, glass));
//    ShapeSet.AddShape(new Sphere(new ExpendVector(-4, 1, 0), 1, lambertianB));
//    ShapeSet.AddShape(new Sphere(new ExpendVector(4, 1, 0), 1, meta));
//}
//private void SceneFinal2()
//{
//    var lambertianG = new Lambertian(new Color(0.5));
//    var glass = new Glass(Color.WHITE, 1.50, 0);
//    var lambertianB = new Lambertian(new Color(0.4, 0.2, 0.1));
//    var meta = new Metal(new Color(0.7, 0.6, 0.5), 0.0);

//    ShapeSet.AddShape(new Sphere(new Point(0, -1000, 0), 1000, lambertianG));

//    var rom = new Random(1337);
//    var stp = new Point(4, 0.2, 0);

//    for (var i = -11; i < 11; i++)
//    {
//        for (var j = -11; j < 11; j++)
//        {
//            var flag = rom.NextDouble();
//            var c = new Point(i + 0.9 * rom.NextDouble(), 0.2, j + 0.9 * rom.NextDouble());

//            if ((c - stp).Length() <= 0.9) continue;

//            Material mat = flag switch
//            {
//                < 0.80 => new Lambertian(new Color(rom.NextDouble(), rom.NextDouble(), rom.NextDouble()) *
//                                         new Color(rom.NextDouble(), rom.NextDouble(), rom.NextDouble())),
//                < 0.95 => new Metal(new Color(rom.NextDouble() / 2 + 0.5, rom.NextDouble() / 2 + 0.5, rom.NextDouble() / 2 + 0.5), rom.NextDouble() * 0.5),
//                _ => glass
//            };
//            ShapeSet.AddShape(new Sphere(c, c + new Point(0, rom.NextDouble() * 0.5, 0), 0.2, mat));
//        }
//    }
//    ShapeSet.AddShape(new Sphere(new Point(0, 1, 0), 1, glass));
//    ShapeSet.AddShape(new Sphere(new Point(-4, 1, 0), 1, lambertianB));
//    ShapeSet.AddShape(new Sphere(new Point(4, 1, 0), 1, meta));
//}
//private void Scene6()
//{
//    var tex = new CheckerboardTex(0.32, new Color(0.2, 0.3, 0.1), new Color(0.9));
//    var lambertianG = new Lambertian(tex);
//    var glass = new Glass(ExpendVector.WHITE, 1.50, 0);
//    var lambertianB = new Lambertian(new Color(0.4, 0.2, 0.1));
//    var meta = new Metal(new Color(0.7, 0.6, 0.5), 0.0);

//    ShapeSet.AddShape(new Sphere(new Point(0, -1000, 0), 1000, lambertianG));

//    var rom = new Random(1337);
//    var stp = new ExpendVector(4, 0.2, 0);

//    for (var i = -11; i < 11; i++)
//    {
//        for (var j = -11; j < 11; j++)
//        {
//            var flag = rom.NextDouble();
//            var c = new ExpendVector(i + 0.9 * rom.NextDouble(), 0.2, j + 0.9 * rom.NextDouble());

//            if ((c - stp).Length() <= 0.9) continue;

//            Material mat = flag switch
//            {
//                < 0.80 => new Lambertian(new ExpendVector(rom.NextDouble(), rom.NextDouble(), rom.NextDouble()) *
//                                         new ExpendVector(rom.NextDouble(), rom.NextDouble(), rom.NextDouble())),
//                < 0.95 => new Metal(new ExpendVector(rom.NextDouble() / 2 + 0.5, rom.NextDouble() / 2 + 0.5, rom.NextDouble() / 2 + 0.5), rom.NextDouble() * 0.5),
//                _ => glass
//            };
//            ShapeSet.AddShape(new Sphere(c, 0.2, mat));
//        }
//    }


//    ShapeSet.AddShape(new Sphere(new ExpendVector(0, 1, 0), 1, glass));
//    ShapeSet.AddShape(new Sphere(new ExpendVector(-4, 1, 0), 1, lambertianB));
//    ShapeSet.AddShape(new Sphere(new ExpendVector(4, 1, 0), 1, meta));
//}
//private void Scene7()
//{
//    _cam = new PerspectiveCamera(W, H, new(13, 2, 3), new(0, 0, 0), new(0, 1, 0), 20, 0, 14);
//    var tex = new CheckerboardTex(0.32, new Color(0.2, 0.3, 0.1), new Color(0.9));
//    var lambertianG = new Lambertian(tex);

//    ShapeSet.AddShape(new Sphere(new Point(0, -10, 0), 10, lambertianG));
//    ShapeSet.AddShape(new Sphere(new Point(0, 10, 0), 10, lambertianG));
//}
//private void Scene8()
//{
//    _cam = new PerspectiveCamera(W, H, new(0, 0, 12), new(0, 0, 0), new(0, 1, 0), 20, 0, 12);

//    var tex = new ImgTex("earthmap.jpg");
//    var tex2 = new SolidTex(new Color(0.7, 0.2, 0.2));
//    var lambertianG = new Lambertian(new Checkerboard3DTex(0.05, tex, tex2));

//    ShapeSet.AddShape(new Sphere(new Point(0, 0, 0), 2, lambertianG));
//}
//private void Scene9()
//{
//    _cam = new PerspectiveCamera(W, H, new(13, 2, 3), new(0, 0, 0), new(0, 1, 0), 20, 0, 14);
//    var tex = new NoiseTex(new PerlinNoise(true, 7), 4);
//    var lambertianG = new Lambertian(tex);

//    ShapeSet.AddShape(new Sphere(new Point(0, 2, 0), 2, lambertianG));
//    ShapeSet.AddShape(new Sphere(new Point(0, -1000, 0), 1000, lambertianG));
//}

//private List<IShape> SceneFinal2()
//{
//    var ans = new List<IShape>();

//    var lambertianG = new Lambertian(new Color(0.5f));
//    var glass = new Glass(Color.One, 1.50f, 0.7f);
//    var lambertianB = new Lambertian(new Color(0.4f, 0.2f, 0.1f));
//    var meta = new Metal(new Color(0.7f, 0.6f, 0.5f), 0.0f);

//    ans.Add(new Sphere(new Point(0, -1000, 0), 1000, lambertianG));

//    var stp = new Point(4, 0.2f, 0);

//    //for (var i = -11; i < 11; i++)
//    //{
//    //    for (var j = -11; j < 11; j++)
//    //    {
//    //        var flag = Utils.Generator.NextSingle();
//    //        var c = new Point(i + 0.9f * Utils.Generator.NextSingle(), 0.2f, j + 0.9f * Utils.Generator.NextSingle());

//    //        if ((c - stp).Length() <= 0.9) continue;

//    //        Material mat = flag switch
//    //        {
//    //            < 0.80f => new Lambertian(Sampler.UniformSample3D(0, 1) * Sampler.UniformSample3D(0, 1)),
//    //            < 0.95f => new Metal(Sampler.UniformSample3D(0.5f, 1), Utils.Generator.NextSingle() * 0.5f),
//    //            _ => glass
//    //        };
//    //        ans.Add(new Sphere(c, c + new Point(0, Utils.Generator.NextSingle() * 0.5f, 0), 0.2f, mat));
//    //    }
//    //}
//    ans.Add(new Sphere(new Point(0, 1, 0), 1, glass));
//    ans.Add(new Sphere(new Point(-4, 1, 0), 1, lambertianB));
//    ans.Add(new Sphere(new Point(4, 1, 0), 1, meta));

//    return ans;
//}
//private List<IShape> Scene11()
//{
//    var ans = new List<IShape>();

//    _cam = new PerspectiveCamera(W, H, new Point( 26, 3, 6), new Point( 0, 2, 0), new Vector(0, 1, 0), 20, 0, 10);
//    _bg = Color.Zero;

//    var mat = new Lambertian(new NoiseTex(new PerlinNoise(true, 7), 4));
//    //var lit = new DiffuseLight(new Color(8));
//    var matN = new Lambertian(new Color(0.8f, 0.2f, 0.4f));

//    ans.Add(new Sphere(new Point(0, -1000, 0), 1000, mat));
//    ans.Add(new Sphere(new Point(0, 2, 0), new Point(0, 4, 0), 2, matN));
//    //ans.Add(new Quad(new Point(-3, 1, -2), new Vector(8, 0, 0), new Vector(0, 8, 0), lit));
//    //IShape box1 = Instance.Box(new Point(0, 0, 0), new Point(1, 3, 1), mat);
//    //box1 = new Rotation(box1, 45);
//    //ans.Add(new Homogeneity(box1, 0.01f, ExpendVector.WHITE));

//    return ans;
//}

//private List<IShape> Scene12()
//{
//    var ans = new List<IShape>();

//    _cam = new PerspectiveCamera(W, H, new Point(278, 278, -800), new Point(278, 278, 0), new Vector(0, 1, 0), 40, 0, 277);
//    _bg = Color.Zero;

//    var matR = new Lambertian(new Color(0.65f, 0.05f, 0.05f));
//    var matG = new Lambertian(new Color(0.12f, 0.45f, 0.15f));
//    var matW = new Lambertian(new Color(0.73f));
//    //var matL = new DiffuseLight(new Color(15));

//    ans.Add(new Quad(new Point(555, 0, 0), new Vector(0, 555, 0), new Vector(0, 0, 555), matG));
//    ans.Add(new Quad(new Point(0, 0, 0), new Vector(0, 555, 0), new Vector(0, 0, 555), matR));
//    //ans.Add(new Quad(new Point(  555, 554, 555), new Vector(-555, 0, 0), new Vector(0, 0, -555), matL));
//    //ans.Add(new Quad(new Point(343, 554, 332), new Vector(-130, 0, 0), new Vector(0, 0, -105), matL));
//    ans.Add(new Quad(new Point(0, 0, 0), new Vector(555, 0, 0), new Vector(0, 0, 555), matW));
//    ans.Add(new Quad(new Point(555, 555, 555), new Vector(-555, 0, 0), new Vector(0, 0, -555), matW));
//    ans.Add(new Quad(new Point(0, 0, 555), new Vector(555, 0, 0), new Vector(0, 555, 0), matW));

//    IShape box1 = Instance.Box(new Point(0, 0, 0), new Point(165, 330, 165), matW);
//    box1 = new Rotation(box1, new Vector(0, 15, 0));
//    box1 = new Translate(box1, new Vector(265, 0, 295));

//    IShape box2 = Instance.Box(new Point(0, 0, 0), new Point(165, 165, 165), matW);
//    box2 = new Rotation(box2, new Vector(0, -18, 20));
//    box2 = new Translate(box2, new Vector(130, 0, 65));
//    //ans.Add(new Homogeneity(box1, 0.01f, ExpendVector.BLACK));
//    //ans.Add(new Homogeneity(box2, 0.01f, ExpendVector.WHITE));
//    ans.Add(box1);
//    ans.Add(box2);

//    return ans;
//}

//private List<IShape> Scene13()
//{
//    var ans = new List<IShape>();

//    _cam = new PerspectiveCamera(W, H, new Point( 278, 278, -800), new Point( 278, 278, 0), new Vector(0, 1, 0), 40, 0, 277);
//    _bg = Color.Zero;

//    var matR = new Lambertian(new Color(0.65f, 0.05f, 0.05f));
//    var matG = new Lambertian(new Color(0.12f, 0.45f, 0.15f));
//    var matW = new Lambertian(new Color(0.73f));
//    var matM = new Glass(new Color(1.0f), 1.5f, 0.3f);
//    //var matL = new DiffuseLight(new Color(9));

//    ans.Add(new Quad(new Point(555, 0, 0), new Vector(0, 555, 0), new Vector(0, 0, 555), matG));
//    ans.Add(new Quad(new Point(  0, 0, 0), new Vector(0, 555, 0), new Vector(0, 0, 555), matR));
//    //ans.Add(new Quad(new Point(  555, 554, 555), new Vector(-555, 0, 0), new Vector(0, 0, -555), matL));
//    //ans.Add(new Quad(new Point(343, 554, 332), new Vector(-130, 0, 0), new Vector(0, 0, -105), matL));
//    ans.Add(new Quad(new Point(  0, 0, 0), new Vector(555, 0, 0), new Vector(0, 0, 555), matW));
//    ans.Add(new Quad(new Point(  555, 555, 555), new Vector(-555, 0, 0), new Vector(0, 0, -555), matW));
//    ans.Add(new Quad(new Point(0, 0, 555), new Vector(555, 0, 0), new Vector(0, 555, 0), matW));

//    IShape box1 = new Sphere(new Point(0, 0, 0), 165, matM);
//    box1 = new Translate(box1, new Vector(265, 165, 295));

//    //IShape box2 = Instance.Box(new Point(0, 0, 0), new Point(165, 165, 165), matW);
//    //box2 = new Rotation(box2, -18);
//    //box2 = new Translate(box2, new Vector(130, 0, 65));
//    ans.Add(box1);
//    //ans.Add(new Homogeneity(box2, 0.01f, ExpendVector.WHITE));

//    return ans;
//}

//private List<IShape> FinalScene2()
//{
//    var ans = new List<IShape>();
//    var b1 = new List<IShape>();
//    var b2 = new List<IShape>();

//    _cam = new PerspectiveCamera(W, H, new Point(478, 278, -600), new Point(278, 278, 0), new Vector(0, 1, 0), 40, 0, 3);
//    _bg = Color.Zero;

//    var lambertianG = new Lambertian(new Color(0.48f, 0.83f, 0.53f));

//    const int mount = 20;
//    const float w = 100.0f;
//    for (var i = 0; i < mount; i++)
//    {
//        for (var j = 0; j < mount; j++)
//        {
//            var x0 = -1000.0f + i * w;
//            var y0 = 0.0f;
//            var z0 = -1000.0f + j * w;
//            var x1 = x0 + w;
//            var y1 = Utils.Generator.Next(1, 101);
//            var z1 = z0 + w;

//            b1.Add(Instance.Box(new Point(x0, y0, z0), new Point(x1, y1, z1), lambertianG));
//        }
//    }

//    ans.Add(new Instance(b1));

//    //var matL = new DiffuseLight(new Color(8));
//    //ans.Add(new Quad(new Point(123, 554, 147), new Vector(300, 0, 0), new Vector(0, 0, 265), matL));

//    var c1 = new Point(400, 400, 200);
//    var c2 = new Point(430, 0, 0);
//    var matS = new Lambertian(new Color(0.7f, 0.3f, 0.1f));
//    var matG = new Glass(Color.One, 1.5f, 0);

//    ans.Add(new Sphere(c1, c2, 50, matS));

//    ans.Add(new Sphere(new Point(260, 150, 45), 50, matG));
//    ans.Add(new Sphere(new Point(0, 150, 145), 50, new Metal(new Color(0.8f, 0.8f, 0.9f), 1.0f)));

//    var s1 = new Sphere(new Point(360, 150, 145), 70, matG);

//    ans.Add(s1);
//    ans.Add(new Homogeneity(s1, 0.2f, new Color(0.2f, 0.4f, 0.9f)));
//    ans.Add(new Homogeneity(new Sphere(new Point(0), 5000, matG), 0.0001f, Color.One));

//    var matI = new Lambertian(new ImgTex("earthmap.jpg"));
//    var matP = new Lambertian(new NoiseTex(new PerlinNoise(true, 7), 0.2f));

//    ans.Add(new Sphere(new Point(400, 200, 400), 100, matI));
//    ans.Add(new Sphere(new Point(220, 280, 300), 80, matP));

//    var matW = new Lambertian(new Color(0.73f));

//    for (var j = 0; j < 1000; j++) {
//        b2.Add(new Sphere(Sampler.UniformSample3D(0, 165), 10, matW));
//    }

//    ans.Add(new Translate(new Rotation(new Instance(b2), new Vector(0, 15, 0)), new Vector(-100, 270, 395)));

//    return ans;
//}