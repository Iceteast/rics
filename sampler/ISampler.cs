﻿using System.Numerics;
using RendererInCSharp.utils;

namespace RendererInCSharp.sampler
{
    public struct Sample
    {
        public Vector Direction;
        public float PDF;
    }

    public class PDFSampler
    {
        public static void ONB(Vector n, out Vector b1, out Vector b2)
        {
            var sign = float.CopySign(1.0f, n.Z);
            var a = -1.0f / (sign + n.Z);
            var b = n.X * n.Y * a;
            b1 = new Vector(1.0f + sign * n.X * n.X * a, sign * b, -sign * n.X);
            b2 = new Vector(b, sign + n.Y * n.Y * a, -n.Y);
        }

        public static float UniformSpherePDF()
        {
            return 1.0f / (4.0f * Utils.PI);
        }
        public static Sample UniformSphereSample()
        {
            var UV = Sampler.UniformSample2D(0, 1);
            var cos = 2.0f * UV.Y - 1.0f;
            var sin = float.Sqrt(1.0f - cos * cos);
            var phi = 2.0f * Utils.PI * UV.X;

            return new Sample
            {
                Direction = new Vector(sin * float.Cos(phi), sin * float.Sin(phi), cos),
                PDF = UniformSpherePDF()
            };
        }

        public static float CosineSpherePDF(float cos)
        {
            return cos / Utils.PI;
        }

        public static Sample CosineSphereSample(Vector normal)
        {
            var UV = Sampler.UniformSample2D(0, 1);
            var phi = 2.0f * UV.X * Utils.PI;
            var x = float.Cos(phi) * float.Sqrt(UV.Y);
            var y = float.Sin(phi) * float.Sqrt(UV.Y);
            var z = float.Sqrt(1 - UV.Y);
            ONB(normal, out var b1, out var b2);
            var dir = x * b1 + y * b2 + z * normal;

            return new Sample
            {
                Direction = dir,
                PDF = CosineSpherePDF(z)
            }; // <--- This is probably incorrect
        }
    }

    public class Sampler
    {
        public static Vector2 UniformSample2D(float min, float max)
        {
            var length = max - min;
            return new Vector2(Utils.Generator.NextSingle() * length + min, Utils.Generator.NextSingle() * length + min);
        }

        public static Vector2 DiskSample2D()
        {
            while (true)
            {
                var ans = UniformSample2D(-1, 1);
                if (ans.Length() < 1.0f)
                    return ans;
            }
        }

        public static Vector2 StratifiedSquareSample2D(int i, int j, float scale)
        {
            return new Vector2((i + Utils.Generator.NextSingle()) * scale - 0.5f,
                               (j + Utils.Generator.NextSingle()) * scale - 0.5f);
        }

        public static Vector UniformSample3DNormalized(float min, float max)
        {
            return Vector.Normalize(UniformSample3D(min, max));
        }

        public static Vector UniformSample3D(float min, float max)
        {
            var length = max - min;
            return new Vector(Utils.Generator.NextSingle() * length + min, 
                              Utils.Generator.NextSingle() * length + min,
                              Utils.Generator.NextSingle() * length + min);
        }

        public static Vector SphereSample3D()     // TODO exchange to double[3] may be faster.
        {
            while (true)
            {
                var ans = new Vector(Utils.Generator.NextSingle() * 2 - 1, Utils.Generator.NextSingle() * 2 - 1, Utils.Generator.NextSingle() * 2 - 1);
                if (ans.LengthSquared() < 1.0)
                    return Vector.Normalize(ans);
            }
        }
        public static Vector HemisphereSample3D(Vector normal)
        {
            var s = SphereSample3D();
            
            return Vector.Dot(s, normal) > 0 ? s : -s;
        }
    }

}
