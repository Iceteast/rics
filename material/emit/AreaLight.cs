﻿using RendererInCSharp.sampler;
using RendererInCSharp.shape;
using RendererInCSharp.texture;

namespace RendererInCSharp.material.emit
{
    internal class AreaLight : ILight
    {
        public AreaLight(Color intensity)
        {
            _tex = new SolidTex(intensity);
        }
        public AreaLight(ITexture tex) 
        {
            _tex = tex;
        }

        public DirectSample SampleDirection(ShapeSample sample, Vector from)
        {
            var dir = from - sample.SamplePoint;
            var cos = Vector.Dot(Vector.Normalize(dir), sample.Normal);

            return new DirectSample
            {
                Cosine = cos,
                Intensity = _tex.GetColor(sample.U, sample.V, sample.SamplePoint) / sample.PDFArea,
                PDFArea = sample.PDFArea,
                PDFDirection = PDFSampler.CosineSpherePDF(cos),
                Position = sample.SamplePoint
            };
        }

        public EmissionSample SampleEmission(ShapeSample sample)
        {
            var pdfSample = PDFSampler.CosineSphereSample(sample.Normal);
            return new EmissionSample
            {
                Position = sample.SamplePoint,
                Cosine = Vector.Dot(pdfSample.Direction, sample.Normal),
                Direction = pdfSample.Direction,
                Intensity = _tex.GetColor(sample.U, sample.V, sample.SamplePoint) / sample.PDFArea,
                PDFArea = sample.PDFArea,
                PDFDirection = pdfSample.PDF
            };
        }

        public EmissionValue Emission(Vector direction, HitRec rec)
        {
            var pdf = PDFSampler.CosineSpherePDF(Vector.Dot(direction, rec.Normal));

            return pdf > 0
                ? new EmissionValue { Intensity = _tex.GetColor(rec.U, rec.V, rec.HitPoint), PDFDirection = pdf } //TODO : PDFArea removed!!!!!
                : new EmissionValue { Intensity = Color.Zero, PDFDirection = 1.0f };
        }

        public bool HasArea()
        {
            return true;
        }

        private readonly ITexture _tex;
    }
}
