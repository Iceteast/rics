﻿using RendererInCSharp.utils;

namespace RendererInCSharp.texture
{
    /// <summary>
    /// Linear Texture. Texture in 1D.
    /// </summary>
    /// <param name="a">coefficient on u axis</param>
    /// <param name="b">coefficient on v axis</param>
    /// <param name="c1">Color for interpolation</param>
    /// <param name="c2">Color for interpolation</param>
    internal class LinearTex(float a, float b, Color c1, Color c2)
        : ITexture
    {
        public Color GetColor(float u, float v, Point p)
        {
            return Interpolation.Interpolation1D(u * a + v * b, c1, c2);
        }
    }
}
