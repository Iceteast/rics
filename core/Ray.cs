﻿
namespace RendererInCSharp.core
{
    public class Ray
    {
        public Ray()
        {
            Origin = new Point(0.0f);
            Direction = new Point(0.0f);
            Time = 0;
        }
        public Ray(Point o, Point d, float tm)
        {
            Origin = o;
            Direction = d;
            Time = tm;
        }

        public Ray(Ray r)
        {
            Origin = r.Origin;
            Direction = r.Direction;
            Time = r.Time;
        }

        public Point At(float t) {
            return Origin + t * Direction;
        }

        public override string ToString()
        {
            return $"[Ray][{Origin}->{Direction}][Time={Time}]";
        }

        public Point Origin, Direction;
        public float Time;

    }
}

