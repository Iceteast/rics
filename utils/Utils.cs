﻿namespace RendererInCSharp.utils;

/// <summary>
/// Utils functions and variables set.
/// </summary>
internal class Utils
{
    /// <summary>
    /// Get the Radians.
    /// </summary>
    /// <param name="degree">Degree value in float.</param>
    /// <returns>Radians</returns>
    public static float DegreeToRadian(float degree)
    {
        return degree * PI / 180.0f;
    }

    /// <summary>
    /// Get the Radians in each axis.
    /// </summary>
    /// <param name="degree">Degree value in three axis.</param>
    /// <returns>Triple Radians in Vector</returns>
    public static Vector DegreeToRadian(Vector degree)
    {
        return degree * PI / 180.0f;
    }

    /// <summary>
    /// Convert RGB24 color to Vector3.
    /// </summary>
    /// <param name="c">Color in RGB24</param>
    /// <returns>Color in Vector3</returns>
    public static Color ConvertColor(Rgb24 c)
    {
        return new Color(c.R, c.G, c.B);
    }

    /// <summary>
    /// Convert Vector3 color to RGB24. with GammaCorrect.
    /// </summary>
    /// <param name="c">Color in Vector3</param>
    /// <returns>Color in RGB24</returns>
    public static Rgb24 ConvertColor(Color c)
    {
        Interval intensity = new(0.000f, 0.999f);

        var ir = (byte)(256 * intensity.Clamp(LinearToGamma(c.X)));
        var ig = (byte)(256 * intensity.Clamp(LinearToGamma(c.Y)));
        var ib = (byte)(256 * intensity.Clamp(LinearToGamma(c.Z)));

        return new Rgb24(ir, ig, ib);
    }

    /// <summary>
    /// Gamma Correction
    /// </summary>
    /// <param name="x">Gamma coefficient</param>
    /// <returns>Color Value in particular channel</returns>
    private static float LinearToGamma(float x) //Gamma 2
    {
        if (x > 0)
        {
            return float.Sqrt(x);
        }
        return 0;
    }

    /// <summary>
    /// Naive Russian Roulette Function.
    /// </summary>
    /// <param name="c">Current Illumination</param>
    /// <param name="threshold">Threshold to be executed.</param>
    /// <returns>The probability</returns>
    public static float RussianRoulette(Color c, float threshold)
    {
        return float.Min(threshold, Vector.Dot(c, Rgb2Gray) * 2.0f);
    }

    /// <summary>
    /// Mean Square Error between Two <see cref="Rgb24"/> Images.
    /// </summary>
    /// <param name="a">One Image</param>
    /// <param name="b">Another Image</param>
    /// <returns>Mean Square Error Value</returns>
    public static float MSE(Image<Rgb24> a, Image<Rgb24> b)
    {
        if (a.Size != b.Size) return float.NaN;
        var ans = 0.0f;
        for (var i = 0; i < a.Width; i++)
        {
            for (var j = 0; j < a.Height; j++)
            {
                ans += (a[i, j].R - b[i, j].R) * (a[i, j].R - b[i, j].R) 
                     + (a[i, j].G - b[i, j].G) * (a[i, j].G - b[i, j].G)
                     + (a[i, j].B - b[i, j].B) * (a[i, j].B - b[i, j].B);
            }
        }
        return ans / (255.0f * 255.0f) / (a.Width * a.Height);
    }

    // Global Constants
    /// <summary>
    /// Global Thread-Safe Random Generator.
    /// </summary>
    public static readonly Random Generator = Random.Shared;
    /// <summary>
    /// Just PI.
    /// </summary>
    public const float PI = (float)Math.PI;
    /// <summary>
    /// Smallest accepted Error
    /// </summary>
    public const float EPSILON = 10e-5f;
    /// <summary>
    /// Magic Number for RGB to Gray.
    /// </summary>
    private static readonly Color Rgb2Gray = new(0.2126f, 0.7152f, 0.0722f);
}
