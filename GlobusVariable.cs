﻿// 3rd Import
global using SixLabors.ImageSharp;
global using SixLabors.ImageSharp.PixelFormats;

// user-define name
global using Color  = System.Numerics.Vector3;
global using Vector = System.Numerics.Vector3;
global using Point  = System.Numerics.Vector3;
global using Trace  = System.Diagnostics.Trace;
