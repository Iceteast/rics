﻿namespace RendererInCSharp.utils;

/// <summary>
/// expend class for <see cref="Vector"/>. 
/// </summary>
public static class ExpendVector
{
    /// <summary>
    /// "Deep copy" the Vector.
    /// </summary>
    /// <param name="me">origin Vector</param>
    /// <returns>new Vector with same attributes</returns>
    public static Color Copy(this Color me)
    {
        return new Color(me.X, me.Y, me.Z);
    }

    /// <summary>
    /// Refraction function.
    /// </summary>
    /// <param name="me">incoming direction</param>
    /// <param name="normal">normal on surface</param>
    /// <param name="etaRate">ratio of two medias</param>
    /// <returns>out-coming direction</returns>
    public static Vector Refract(this Vector me, Vector normal, float etaRate)
    {
        var cosTheta = float.Min(Vector.Dot(-me, normal), 1.0f);
        var rOutPerp = etaRate * (me + cosTheta * normal);
        var rOutParallel = -float.Sqrt(float.Abs(1.0f - rOutPerp.LengthSquared())) * normal;
        return rOutPerp + rOutParallel;
    }
}


