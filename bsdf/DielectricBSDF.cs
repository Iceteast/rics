﻿using RendererInCSharp.sampler;
using RendererInCSharp.shape;
using RendererInCSharp.utils;

namespace RendererInCSharp.bsdf
{
    internal class DielectricBSDF : BSDF
    {
        public DielectricBSDF(Color attenuation, float etaRate, float fuzz)
        {
            _fuzz = fuzz;
            _etaRate = etaRate;
            Attenuation = attenuation;
            IsSpecular = true;
        }

        public override BSDFSample Sample(HitRec rec, Vector dir)
        {
            var k = _etaRate;
            var cosThetaI = Vector.Dot(Vector.Normalize(-dir), Vector.Normalize(rec.Normal));

            if (cosThetaI < 0)
            {
                k = 1.0f / k;
                cosThetaI *= -1;
                rec.Normal *= -1;
            }

            var cosThetaTSquare = float.Clamp(1.0f - k * k * (1.0f - cosThetaI * cosThetaI), 0.0f, 1.0f);

            // perfect reflection
            if (cosThetaTSquare < Utils.EPSILON)
                return new BSDFSample(Vector.Reflect(dir, rec.Normal) + _fuzz * Sampler.SphereSample3D(), 1.0f, Attenuation);
            var nt = (k * cosThetaI - float.Sqrt(cosThetaTSquare)) * rec.Normal + k * dir;
            return Utils.Generator.NextSingle() > Fresnel.Factor(k, cosThetaI, float.Sqrt(cosThetaTSquare)) ?
                // Refraction
                //new BSDFSample(nt, 1.0f, Attenuation) :
                new BSDFSample(dir.Refract(rec.Normal, k), 1.0f, Attenuation) :
                // Reflection
                new BSDFSample(Vector.Reflect(dir, rec.Normal) + _fuzz * Sampler.SphereSample3D(), 1.0f, Attenuation);

            //var k = rec.FrontFace ? _etaRate : 1.0f / _etaRate;
            //var cosThetaI = Vector.Dot(Vector.Normalize(-dir), Vector.Normalize(rec.Normal));
            //var cosThetaTSquare = float.Clamp(1.0f - k * k * (1.0f - cosThetaI * cosThetaI), 0.0f, 1.0f);
            //if (cosThetaTSquare > 0) {
            //    // Refraction
            //    var cosThetaT = float.Sqrt(cosThetaTSquare);
            //    var F = Fresnel.Factor(k, cosThetaI, cosThetaT);
            //    if (Utils.Generator.NextSingle() > F) {
            //        var nt =  (k * cosThetaI - cosThetaTSquare) * rec.Normal + k * dir;
            //        return new BSDFSample(nt, 1.0f, Attenuation);
            //    }
            //}

            //// Reflection
            //return new BSDFSample(Vector.Reflect(dir, rec.Normal) + _fuzz * Sampler.SphereSample3D(), 1.0f, Attenuation);
        }


        public override float PDF(Vector wIn, HitRec rec, Vector wOut)
        {
            throw new NotImplementedException();
        }

        public override Color Eval(Vector wIn, HitRec rec, Vector wOut)
        {
            throw new NotImplementedException();
        }

        private readonly float _etaRate, _fuzz;
    }
}
