﻿namespace RendererInCSharp.texture;

/// <summary>
/// Constant Texture.
/// </summary>
/// <param name="albedo">Color</param>
internal class SolidTex(Color albedo) : ITexture
{
    public Color GetColor(float u, float v, Color p)
    {
        return albedo;
    }
}

