﻿using RendererInCSharp.bsdf;
using RendererInCSharp.core;
using RendererInCSharp.material;
using RendererInCSharp.texture;
using RendererInCSharp.utils;

namespace RendererInCSharp.shape.transform
{
    internal class Homogeneity : IShape //TODO: IShape is redundant. Don't need Mat of skeleton.
    {
        public Homogeneity(IShape skeleton, float density, ITexture tex)
        {
            _shape = skeleton;
            _density = density;
            Mat = new Material(new IsotropicBSDF(tex));
            BBox = _shape.BBox;
        }

        public Homogeneity(IShape skeleton, float density, Color albedo)
        {
            _shape = skeleton;
            _density = density;
            Mat = new Material(new IsotropicBSDF(albedo));
            BBox = _shape.BBox;
        }

        public bool Hit(Ray r, Interval t, ref HitRec rec)
        {
            var rec1 = new HitRec();
            var rec2 = new HitRec();

            if (!_shape.Hit(r, Interval.FULL, ref rec1)) return false;

            if (!_shape.Hit(r, new Interval(rec1.T + Utils.EPSILON, float.PositiveInfinity), ref rec2)) return false;

            if (rec1.T < t.Min) rec1.T = t.Min;
            if (rec2.T > t.Max) rec2.T = t.Max;

            if (rec1.T >= rec2.T) return false;

            if (rec1.T < 0) rec1.T = 0;

            var rayLength = r.Direction.Length();
            var distanceInside = (rec2.T - rec1.T) * rayLength;
            var hitDistance = float.Log2(Utils.Generator.NextSingle()) / -_density;

            if (hitDistance > distanceInside) return false;

            rec.T = rec1.T + hitDistance / rayLength;
            rec.HitPoint = r.At(rec.T);

            rec.Normal = new Vector(1, 0, 0);
            rec.FrontFace = true;
            rec.Mat = Mat;

            return true;

        }

        public ShapeSample Sample()
        {
            return _shape.Sample();
        }

        public float GetArea()
        {
            return _shape.GetArea();
        }

        private readonly IShape _shape;
        private readonly float _density;

        public AABB BBox { get; set; }
        public Material Mat { get; set; }
    }
}
