﻿using RendererInCSharp.sampler;
using RendererInCSharp.shape;
using RendererInCSharp.texture;
using RendererInCSharp.utils;

namespace RendererInCSharp.bsdf
{
    internal class IsotropicBSDF : BSDF
    {
        public IsotropicBSDF(ITexture tex)
        {
            _tex = tex;
        }
        public IsotropicBSDF(Color albedo)
        {
            _tex = new SolidTex(albedo);
        }
        public override BSDFSample Sample(HitRec rec, Vector dir)
        {
            return new BSDFSample(Sampler.SphereSample3D(), 1.0f / (4.0f * Utils.PI), _tex.GetColor(rec.U, rec.V, rec.HitPoint));
        }

        public override float PDF(Vector wIn, HitRec rec, Vector wOut)
        {
            throw new NotImplementedException();
        }

        public override Color Eval(Vector wIn, HitRec rec, Vector wOut)
        {
            throw new NotImplementedException();
        }

        private readonly ITexture _tex;
    }
}
