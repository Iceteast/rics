﻿using RendererInCSharp.utils;

namespace RendererInCSharp.texture;

/// <summary>
/// Image Texture.
/// </summary>
/// <param name="filename">Image file</param>
internal class ImgTex(string filename) : ITexture
{
    public Color GetColor(float u, float v, Point p)
    {
        u = new Interval(0, 1).Clamp(u);
        v = 1.0f - new Interval(0, 1).Clamp(v);
        var i = (int)(u * (_img.Width - 1));
        var j = (int)(v * (_img.Height - 1));

        return Utils.ConvertColor(_img[i, j]) / 255.0f;
    }

    private readonly Image<Rgb24> _img = (Image<Rgb24>)Image.Load(filename);
}
