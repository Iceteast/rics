﻿using RendererInCSharp.bsdf;
using RendererInCSharp.core;
using RendererInCSharp.sampler;
using RendererInCSharp.shape;
using RendererInCSharp.texture;

//namespace RendererInCSharp.material.phase
//{
//    internal class Isotropic : Material
//    {
//        public Isotropic(Color albedo)
//        {
//            _tex = new SolidTex(albedo);
//        }

//        public Isotropic(ITexture tex)
//        {
//            _tex = tex;
//        }
//        public bool Scatter(Ray rIn, HitRec rec, ref Color att, ref Ray rOut)
//        {
//            rOut = new Ray(rec.HitPoint, Sampler.SphereSample3D(), rIn.Time);
//            att = _tex.GetColor(rec.U, rec.V, rec.HitPoint);
//            return true;
//        }

//        public bool IsSpecular()
//        {
//            return false;
//        }

//        public ILight? Emitter { get; set; }
//        public BSDF? Bsdf { get; set; }

//        private readonly ITexture _tex;
//    }
//}
