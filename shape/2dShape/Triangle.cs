﻿using RendererInCSharp.material;

namespace RendererInCSharp.shape._2dShape
{
    internal class Triangle: Plane
    {
        public Triangle(Point p, Vector u, Vector v, Material mat) : base(p, u, v, mat)
        {
            BBox = new AABB(p, p + u) + new AABB(p, p + v);
        }

        protected override bool IsInterior(float a, float b, ref HitRec rec)
        {
            if (a < 0 || b < 0 || a + b >= 1) return false;

            rec.U = a;
            rec.V = b;
            return true;
        }
        public override float GetArea()
        {
            return Vector.Cross(U, V).Length() / 2.0f;
        }

        public override string ToString()
        {
            return "Triangle";
        }
    }
}
