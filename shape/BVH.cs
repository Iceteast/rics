﻿using RendererInCSharp.core;
using RendererInCSharp.material;
using RendererInCSharp.utils;

namespace RendererInCSharp.shape;

public class BVH : IShape
{
    public BVH(List<IShape> shapes)
    {
        Mat = Material.NullMaterial; //TODO need to be changed.
        
        var axis = Utils.Generator.Next(0, 3);

        var span = shapes.Count;
        if (span == 1)
        {
            Left = Right = shapes[0];
        } else if (span == 2)
        {
            Left = shapes[0];
            Right = shapes[1];
        }
        else
        {
            shapes.Sort(axis switch
            {
                0 => IShape.CompareShapeByAxisX,
                1 => IShape.CompareShapeByAxisY,
                _ => IShape.CompareShapeByAxisZ
            });
            var mid = span / 2;
            Left = new BVH(shapes.GetRange(0, mid));
            Right = new BVH(shapes.GetRange(mid, shapes.Count - mid));
        }
        BBox = Left.BBox + Right.BBox;
    }
    public bool Hit(Ray r, Interval t, ref HitRec rec)
    {
        if(!BBox.Hit(r, t)) return false;

        var hitL = Left.Hit(r, t, ref rec);
        var hitR = Right.Hit(r, new Interval(t.Min, hitL ? rec.T : t.Max), ref rec);

        return hitL || hitR;
    }

    public ShapeSample Sample() //TODO : uniformly?
    {
        return Utils.Generator.NextSingle() < 0.5f ? Left.Sample() : Right.Sample();
    }

    public float GetArea()
    {
        if (_area < Utils.EPSILON)
        {
            _area = Left.GetArea() + Right.GetArea();
        }
        return _area;
    }

    

    public IShape Left, Right;
    public AABB BBox { get; set; }
    public Material Mat { get; set; } //TODO might be wrong when light is a BVH.

    private float _area;
}
