﻿using RendererInCSharp.sampler;
using RendererInCSharp.shape;

namespace RendererInCSharp.bsdf
{
    internal class SpecularBSDF : BSDF
    {
        public SpecularBSDF(Color c, float fuzz)
        {
            Attenuation = c;
            _fuzz = fuzz;
            IsSpecular = true;

        }
        public override BSDFSample Sample(HitRec rec, Vector dir)
        {
            return new BSDFSample( Vector.Normalize(Vector.Reflect(dir, rec.Normal)) + _fuzz * Sampler.SphereSample3D(), 1.0f, Attenuation);
        }

        public override float PDF(Vector wIn, HitRec rec, Vector wOut)
        {
            throw new NotImplementedException();
        }

        public override Color Eval(Vector wIn, HitRec rec, Vector wOut)
        {
            throw new NotImplementedException();
        }

        private readonly float _fuzz;
    }
}
