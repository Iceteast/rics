﻿using RendererInCSharp.bsdf;

namespace RendererInCSharp.material
{
    public class Material
    {
        public static Material NullMaterial = new();
        public Material() {}

        public Material(BSDF bsdf)
        {
            Bsdf = bsdf;
            Emitter = null;
        }

        public Material(ILight light)
        {
            Bsdf = null;
            Emitter = light;
        }

        public Material(BSDF bsdf, ILight light)
        {
            Bsdf = bsdf;
            Emitter = light;
        }

        public ILight? Emitter;
        public BSDF? Bsdf;
    };
}
