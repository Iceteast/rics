﻿using System.Runtime.InteropServices;
using RendererInCSharp.utils;

namespace RendererInCSharp
{
    /// <summary>
    /// A naive Renderer in C#. For my bachelor thesis.
    /// </summary>
    internal class RiCS
    {
        /// <summary>
        /// Using <see cref="Factory"/> to load an XML Scene.
        /// Warning: Console.Beep only works on Windows.
        /// </summary>
        /// <returns>0</returns>
        private static int Main()
        {
            //Factory.LoadSceneFromXML(@"extern\CornellBoxGlass.xml").RenderPixel(310, 350, 310, 340).SaveAsPng(File.Create(@"NEE.png"));
            Factory.LoadSceneFromXML(@"extern\CornellBoxGlass.xml").Render().SaveAsPng(File.Create(@"NEE.png"));
            //Trace.WriteLine(Utils.MSE(img1, img2));

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) Console.Beep(600, 800);

            return 0;
        }
    }
}
