﻿namespace RendererInCSharp.texture;

/// <summary>
/// Interface of Texture. It gives a basic structure of colors on any surface.
/// </summary>
public interface ITexture
{
    /// <summary>
    /// return Color on a certain location.
    /// </summary>
    /// <param name="u">coordination 2D on surface</param>
    /// <param name="v">coordination 2D on surface</param>
    /// <param name="p">coordination 3D on space</param>
    /// <returns>Texture color.</returns>
    public Color GetColor(float u, float v, Point p);
}
