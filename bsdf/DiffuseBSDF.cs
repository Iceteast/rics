﻿using RendererInCSharp.sampler;
using RendererInCSharp.shape;
using RendererInCSharp.texture;
using RendererInCSharp.utils;

namespace RendererInCSharp.bsdf
{
    internal class DiffuseBSDF : BSDF
    {
        public DiffuseBSDF(ITexture tex)
        {
            _tex = tex;
            IsSpecular = false;
        }
        
        public override BSDFSample Sample(HitRec rec, Vector _)
        {
            var sample = PDFSampler.CosineSphereSample(rec.Normal);
            var att = _tex.GetColor(rec.U, rec.V, rec.HitPoint) * (float.Max(Vector.Dot(sample.Direction, rec.Normal), 0.0f) * Kd);

            return new BSDFSample(sample.Direction, sample.PDF, att);
        }

        public override float PDF(Vector wIn, HitRec rec, Vector wOut)
        {
            return PDFSampler.CosineSpherePDF(float.Max(Vector.Dot(wIn, rec.Normal), 0.0f));
        }

        public override Color Eval(Vector wIn, HitRec rec, Vector wOut)
        {
            return _tex.GetColor(rec.U, rec.V, rec.HitPoint) * Kd;
        }

        private readonly ITexture _tex;
        private const float Kd = 1.0f / Utils.PI; // TODO: what's this??? short for what?
    }
}
