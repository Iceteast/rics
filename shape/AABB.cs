﻿using RendererInCSharp.core;
using RendererInCSharp.utils;

namespace RendererInCSharp.shape
{
    public class AABB
    {
        public AABB()
        {
            AxisX = Interval.EMPTY;
            AxisY = Interval.EMPTY;
            AxisZ = Interval.EMPTY;
        }

        public AABB(Interval x, Interval y, Interval z)
        {
            AxisX = x;
            AxisY = y;
            AxisZ = z;
            EnlargePadding();
        }

        public AABB(Point a, Point b)
        {
            AxisX = new Interval(float.Min(a.X, b.X), float.Max(a.X, b.X));
            AxisY = new Interval(float.Min(a.Y, b.Y), float.Max(a.Y, b.Y));
            AxisZ = new Interval(float.Min(a.Z, b.Z), float.Max(a.Z, b.Z));
            EnlargePadding();
        }

        public Interval IndexAxis(int axis)
        {
            return axis switch
            {
                0 => AxisX,
                1 => AxisY,
                2 => AxisZ,
                _ => Interval.EMPTY
            };
        }

        public AABB Move(Vector offset)
        {
            AxisX += offset.X;
            AxisY += offset.Y;
            AxisZ += offset.Z;
            return this;
        }

        public static AABB operator +(AABB a, AABB b)
        {
            return new AABB(a.AxisX + b.AxisX, a.AxisY + b.AxisY, a.AxisZ + b.AxisZ);
        }

        public bool Hit(Ray r, Interval t) // hit function from Andrew Kensler.
        {
            for (var axis = 0; axis < 3; axis++)
            {

                var ax = IndexAxis(axis);
                var inv = 1.0f / r.Direction[axis];
                var orig = r.Origin[axis];

                var t0 = (ax.Min - orig) * inv;
                var t1 = (ax.Max - orig) * inv;

                if (inv < 0.0f)   (t0, t1) = (t1, t0);
                if (t0 > t.Min)  t.Min = t0;
                if (t1 < t.Max)  t.Max = t1;
                
                if (t.Max <= t.Min) return false;
            }
            return true;
        }

        public override string ToString()
        {
            return $"[BBox]{AxisX.Min},{AxisY.Min},{AxisZ.Min} -> {AxisX.Max},{AxisY.Max},{AxisZ.Max}";
        }

        private void EnlargePadding()
        {
            if (AxisX.Size() < Utils.EPSILON) AxisX.Expand(Utils.EPSILON);
            if (AxisY.Size() < Utils.EPSILON) AxisY.Expand(Utils.EPSILON); 
            if (AxisZ.Size() < Utils.EPSILON) AxisZ.Expand(Utils.EPSILON);
        }
        public Interval AxisX, AxisY, AxisZ;
        
    }
}
