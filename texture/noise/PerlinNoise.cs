﻿using RendererInCSharp.sampler;
using RendererInCSharp.utils;

namespace RendererInCSharp.texture.noise;

internal class PerlinNoise:INoise
{
    public PerlinNoise(bool turbo, int depth)
    {
        _intensity = new List<Point>(Capacity);

        for (var i = 0; i < Capacity; i++)
        {
            _intensity.Add(Sampler.UniformSample3DNormalized(-1, 1));
        }

        _permX = GeneratePermutation();
        _permY = GeneratePermutation();
        _permZ = GeneratePermutation();
        _hasTurbo = turbo;
        _turboDepth = depth;
    }

    public float GetIntensity(Point p)
    {
        return _hasTurbo ? Turbo(p, _turboDepth) : (Noise(p) + 1) * 0.5f;
    }

    /// <summary>
    /// noise artifact.
    /// </summary>
    /// <param name="p"></param>
    /// <returns></returns>
    private float Noise(Point p)
    {
        var u = p.X - float.Floor(p.X);
        var v = p.Y - float.Floor(p.Y);
        var w = p.Z - float.Floor(p.Z);

        var i = (int) float.Floor(p.X); //To avoiding artifact on zeros.
        var j = (int) float.Floor(p.Y);
        var k = (int) float.Floor(p.Z);
        var dd = new Point[2,2,2];

        for (var a = 0; a < 2; a++)
        {
            for (var b = 0; b < 2; b++)
            {
                for (var c = 0; c < 2; c++)
                {
                    dd[a, b, c] = _intensity[_permX[(a + i) & (Capacity - 1)] ^
                                             _permY[(b + j) & (Capacity - 1)] ^
                                             _permZ[(c + k) & (Capacity - 1)]];
                }
            }
        }
        return Interpolation.Interpolation3D(u, v, w, ref dd);
    }

    /// <summary>
    /// turbo artifact.
    /// </summary>
    /// <param name="p"></param>
    /// <param name="depth"></param>
    /// <returns></returns>
    private float Turbo(Point p, int depth)
    {
        var ans = 0.0f;
        var weight = 1.0f;

        for (var i = 0; i < depth; i++)
        {
            ans += weight * Noise(p);
            weight *= 0.5f;
            p *= 2;
        }
        return float.Abs(ans);
    }

    /// <summary>
    /// generate a random permutation of indices.
    /// </summary>
    /// <returns></returns>
    private static List<int> GeneratePermutation() // Might not be safe for threads.
    {
        var ans = new List<int>(Enumerable.Range(0, Capacity));

        for (var i = Capacity - 1; i > 0; i--)
        {
            var t = Utils.Generator.Next(i);
            (ans[i], ans[t]) = (ans[t], ans[i]);
        }
        return ans;
    }

    private readonly List<Point> _intensity;
    private const int Capacity = 256;
    private readonly List<int> _permX, _permY, _permZ;
    private readonly bool _hasTurbo;
    private readonly int _turboDepth;
}

