﻿using RendererInCSharp.core;
using RendererInCSharp.utils;

namespace RendererInCSharp.integrator;

internal class NEE(Scene s) : Integrator(s)
{
    protected override Color GetColor(Ray r, int depth)
    {
        var ans = Color.Zero;
        var acc = Color.One;

        for (var i = 0; i < depth; i++)
        {
            // Missing
            if (!Scene.Hit(r, new Interval(0.0001f, float.MaxValue), out var rec)) break;

            // Light
            if (rec.Mat.Emitter != null && rec.FrontFace) return rec.Mat.Emitter.Emission(-r.Direction, rec).Intensity * acc;

            // Specular
            if (rec.Mat.Bsdf is not { } bsdf) break;

            if (!bsdf.IsSpecular)
            {
                var (shapeSample, randomLight) = Scene.SampleLight();
                var sample = randomLight.SampleDirection(shapeSample, rec.HitPoint);
                var nDir = rec.HitPoint - sample.Position;
                var cosine = Vector.Dot(Vector.Normalize(nDir), rec.Normal);

                if (cosine > 0 && !Scene.Hit(new Ray(sample.Position, nDir, r.Time), new Interval(Utils.EPSILON, nDir.Length()- Utils.EPSILON / 77), out _)) 
                {
                    ans += acc * sample.Intensity / sample.PDFArea * sample.Cosine / Scene.Lights.Count 
                           * rec.Mat.Bsdf.Eval(nDir, rec, -r.Direction) * cosine * nDir.Length() * nDir.Length();
                }
            }

            var sp = Utils.RussianRoulette(acc, 0.75f);
            if (Utils.Generator.NextSingle() > sp) break;

            var s = bsdf.Sample(rec, r.Direction);                     // get the next sample.
            s.PDF *= sp;                                                        // add the RR probability
            s.Intensity /= s.PDF;
            // update acc with hit cosine and color.
            //acc *= s.Intensity * Vector.Dot(Vector.Normalize(s.Direction), rec.Normal) / s.PDF;
            acc *= s.Intensity * float.Abs(Vector.Dot(Vector.Normalize(s.Direction), rec.Normal)); //TODO Abs remove
            r = new Ray(rec.HitPoint, Vector.Normalize(s.Direction), r.Time);                 // update ray
        }

        return ans;
    }
}