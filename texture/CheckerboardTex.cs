﻿namespace RendererInCSharp.texture;

/// <summary>
/// checkerboard Texture in 3D without Interpolation.
/// </summary>
/// <param name="scale">size of block</param>
/// <param name="tex1">Texture on even</param>
/// <param name="tex2">Texture on odd</param>
internal class CheckerboardTex(float scale, ITexture tex1, ITexture tex2) : ITexture
{
    /// <summary>
    /// checkerboard Texture in 3D without Interpolation.
    /// </summary>
    /// <param name="scale">size of block</param>
    /// <param name="c1">Solid Texture on even</param>
    /// <param name="c2">Solid Texture on odd</param>
    public CheckerboardTex(float scale, Color c1, Color c2) : this(scale, new SolidTex(c1), new SolidTex(c2)) {}

    public Color GetColor(float u, float v, Point p)
    {
        var x = (int)float.Floor(p.X / scale);
        var y = (int)float.Floor(p.Y / scale);
        var z = (int)float.Floor(p.Z / scale);

        return (x + y + z) % 2 == 0 ? tex1.GetColor(u, v, p) : tex2.GetColor(u, v, p);
    }
}
