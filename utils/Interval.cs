﻿namespace RendererInCSharp.utils;
/// <summary>
/// Struct for 1D Interval.
/// </summary>
/// <param name="min">Minimum</param>
/// <param name="max">Maximum</param>
public struct Interval(float min, float max)
{
    /// <summary>
    /// Constantly Empty Interval.
    /// </summary>
    public static readonly Interval EMPTY = new(float.PositiveInfinity, float.NegativeInfinity);
    /// <summary>
    /// Constantly Universal Interval.
    /// </summary>
    public static readonly Interval FULL = new(float.NegativeInfinity, float.PositiveInfinity);

    /// <summary>
    /// return the length of Interval.
    /// </summary>
    /// <returns>Max - Min</returns>
    public readonly float Size()
    {
        return Max - Min;
    }

    /// <summary>
    /// return if x in Interval.
    /// </summary>
    /// <param name="x"></param>
    /// <returns>if x in Interval.</returns>
    public readonly bool Contains(float x)
    {
        return x >= Min && x <= Max;
    }

    /// <summary>
    /// return if x is surrounded by Interval.
    /// </summary>
    /// <param name="x"></param>
    /// <returns></returns>
    public readonly bool Surrounds(float x)
    {
        return x > Min && x < Max;
    }

    /// <summary>
    /// clamp x with certain Interval.
    /// </summary>
    /// <param name="x"></param>
    /// <returns>x in Interval</returns>
    public readonly float Clamp(float x)
    {
        return x > Max ? Max : x < Min ? Min : x;
    }

    /// <summary>
    /// Expand the Interval in two directions(each half x).
    /// </summary>
    /// <param name="x">the size need to expand</param>
    /// <returns>the expanded Interval</returns>
    public Interval Expand(float x)
    {
        Min -= x / 2;
        Max += x / 2;
        return this;
    }

    /// <summary>
    /// offset the Interval.
    /// </summary>
    /// <param name="a">origin Interval</param>
    /// <param name="b">offset</param>
    /// <returns>new Interval</returns>
    public static Interval operator +(Interval a, float b)
    {
        return new Interval(a.Min + b, a.Max + b);
    }

    /// <summary>
    /// Create new Interval contains two Intervals.
    /// </summary>
    /// <param name="a"></param>
    /// <param name="b"></param>
    /// <returns></returns>
    public static Interval operator +(Interval a, Interval b)
    {
        return new Interval(float.Min(a.Min, b.Min), float.Max(a.Max, b.Max));
    }

    public readonly override string ToString()
    {
        return $"[{Min}, {Max}]";
    }

    public float Min = min, Max = max;
}

