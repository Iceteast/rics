﻿using System.Diagnostics;
using RendererInCSharp.core;
using RendererInCSharp.utils;

namespace RendererInCSharp.integrator
{
    internal abstract class Integrator(Scene scene)
    {
        /// <summary>
        /// Get the image of a certain scene.
        /// </summary>
        /// <returns>The rendered image.</returns>
        protected abstract Color GetColor(Ray r, int depth);

        public virtual Image<Rgb24> Render()
        {
            var img = new Image<Rgb24>(Scene.Cam.W, Scene.Cam.H);

            var sw = Stopwatch.StartNew();
            Trace.WriteLine("Start rendering...");

            Parallel.For(0, Scene.Cam.H, j =>
            {
                Parallel.For(0, Scene.Cam.W, i =>
                {
                    var intensity = Color.Zero;
                    var rays = Scene.Cam.GetRays(i, j);

                    intensity = rays.Aggregate(intensity, (current, r) => current + GetColor(r, Scene.Cam.Depth)) /
                                rays.Count;

                    img[i, j] = Utils.ConvertColor(intensity);
                });
            });
            Trace.WriteLine($"Render Work Done in {sw.Elapsed.Hours,2:D2}:{sw.Elapsed.Minutes,2:D2}:{sw.Elapsed.Seconds,2:D2}.{sw.Elapsed.Milliseconds,3:D3}.");

            return img;
        }

        public virtual Image<Rgb24> RenderPixel(int x0, int x1, int y0, int y1)
        {
            if (x0 < 0 || y0 < 0 || x1 < 0 || y1 < 0 || x0 > x1 || y0 > y1 || x1 > Scene.Cam.W || y1 > Scene.Cam.H)
                throw new ArgumentOutOfRangeException($"x={x0}-{x1} | y={y0}-{y1}");

            var img = new Image<Rgb24>(x1 - x0, y1 - y0);

            var sw = Stopwatch.StartNew();
            Trace.WriteLine($"Start rendering pixels from ({x0}, {y0}) to ({x1}, {y1})...");

            Parallel.For(y0, y1, j =>
            {
                Parallel.For(x0, x1, i =>
                {
                    var intensity = Color.Zero;
                    var rays = Scene.Cam.GetRays(i, j);

                    intensity = rays.Aggregate(intensity, (current, r) => current + GetColor(r, Scene.Cam.Depth)) /
                                rays.Count;

                    img[i - x0, j - y0] = Utils.ConvertColor(intensity);
                });
            });
            Trace.WriteLine($"Render Work Done in {sw.Elapsed.Hours,2:D2}:{sw.Elapsed.Minutes,2:D2}:{sw.Elapsed.Seconds,2:D2}.{sw.Elapsed.Milliseconds,3:D3}.");

            return img;
        }

        protected readonly Scene Scene = scene;
    }
}
