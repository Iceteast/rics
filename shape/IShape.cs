﻿using RendererInCSharp.core;
using RendererInCSharp.material;
using RendererInCSharp.utils;

namespace RendererInCSharp.shape;

/// <summary>
/// Hitting Record.
/// </summary>
public struct HitRec
{
    /// <summary>
    /// Coordinate of Intersection
    /// </summary>
    public Point HitPoint;

    /// <summary>
    /// Normal of Intersection
    /// </summary>
    public Vector Normal;

    /// <summary>
    /// Material of Intersection surface
    /// </summary>
    public Material Mat;

    /// <summary>
    /// Ray length of Intersection
    /// </summary>
    public float T;

    /// <summary>
    /// Coordinate 2D on Intersection surface
    /// </summary>
    public float U, V;

    /// <summary>
    /// if Hitting on front face.
    /// </summary>
    public bool FrontFace;

    /// <summary>
    /// Correct Normal direction with ray.
    /// </summary>
    /// <param name="r">incoming Ray</param>
    /// <param name="normal">Hit normal</param>
    public void SetFaceNormal(Ray r, Vector normal)
    {
        FrontFace = Vector.Dot(r.Direction, normal) < 0;
        Normal = normal;  //FrontFace ? normal : -normal;
    }

    public readonly override string ToString()
    {
        return $"[HitRec]{HitPoint} -> {Normal} @ ({U}, {V})";
    }
}

/// <summary>
/// A random Sample of Shape.
/// </summary>
/// <param name="u">Coordinate 2D</param>
/// <param name="v">Coordinate 2D</param>
/// <param name="samplePoint">Coordinate 3D</param>
/// <param name="normal">Normal of Shape</param>
/// <param name="pdfArea">Probability of this Sampling behavior</param>
public struct ShapeSample(float u, float v, Point samplePoint, Vector normal, float pdfArea)
{
    public float U = u;
    public float V = v;
    public Point SamplePoint = samplePoint;
    public Vector Normal = normal;
    public float PDFArea = pdfArea;
}

/// <summary>
/// Interface of Shape. Which describes a mathematical shape in Space.
/// </summary>
public interface IShape
{
    public static int CompareShapeByAxisX(IShape a, IShape b)
    {
        return a.BBox.AxisX.Min.CompareTo(b.BBox.AxisX.Min);
    }
    public static int CompareShapeByAxisY(IShape a, IShape b)
    {
        return a.BBox.AxisY.Min.CompareTo(b.BBox.AxisY.Min);
    }
    public static int CompareShapeByAxisZ(IShape a, IShape b)
    {
        return a.BBox.AxisZ.Min.CompareTo(b.BBox.AxisZ.Min);
    }
    
    /// <summary>
    /// returns if a ray hit this shape.
    /// </summary>
    /// <param name="r">incoming ray</param>
    /// <param name="t">the available interval of ray length</param>
    /// <param name="rec">information need to be recorded</param>
    /// <returns></returns>
    bool Hit(Ray r, Interval t, ref HitRec rec);

    /// <summary>
    /// returns a <see cref="ShapeSample"/> of this shape.
    /// </summary>
    /// <returns></returns>
    ShapeSample Sample();

    /// <summary>
    /// returns the area of this shape.
    /// </summary>
    /// <returns></returns>
    float GetArea();

    /// <summary>
    /// BoundingBox
    /// </summary>
    AABB BBox { get; set; }

    /// <summary>
    /// Material
    /// </summary>
    Material Mat { get; set; }
}
