﻿using RendererInCSharp.shape;

namespace RendererInCSharp.bsdf;

public struct BSDFSample(Vector dir, float pdf, Color color)
{
    public Vector Direction = dir;
    public float PDF = pdf;
    public Color Intensity = color;
}

public abstract class BSDF
{
    public abstract BSDFSample Sample(HitRec rec, Vector dir);
    public abstract float PDF(Vector wIn, HitRec rec, Vector wOut);
    public abstract Color Eval(Vector wIn, HitRec rec, Vector wOut);

    //public Ray ROut;
    public Color Attenuation;
    public bool IsSpecular;
}

