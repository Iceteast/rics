﻿using RendererInCSharp.core;
using RendererInCSharp.material;
using RendererInCSharp.utils;

namespace RendererInCSharp.shape._3dShape
{
    internal class Quadric : IShape
    {
        public Quadric(/*Point origin, Vector normal, */float a, float b, float c, float d, Material mat)
        {
            BBox = new AABB();
            _a = a;
            _b = b;
            _c = c;
            _d = d;
            Mat = mat;
        }

        public bool Hit(Ray r, Interval ta, ref HitRec rec)
        {
            var r2 = new Vector(_a * float.Abs(_a), _b * float.Abs(_b), _c * float.Abs(_c)); // abcd.xyz*abs(abcd.xyz)); // squared WITH sign
    
            var k2 = Vector.Dot(r.Direction, r.Direction * r2);
            var k1 = Vector.Dot(r.Direction, r.Origin * r2);
            var k0 = Vector.Dot(r.Origin, r.Origin * r2) - _d;
             
            var h = k1 * k1 - k2 * k0;
            if( h < 0.0f ) return false;
            h = float.Sqrt(h) * float.Sign(k2);

            // entry point
            var t = (-k1 - h) / k2;
            var pos = r.At(t);

            if (!ta.Surrounds(t))
            {
                t = (-k1 + h) / k2;
                if (!ta.Surrounds(t))
                {
                    return false;
                }
            }

            rec.T = t;
            rec.FrontFace = k2 < 0.0;
            rec.Normal = Vector.Normalize(pos * r2);
            rec.HitPoint = r.At(t);
            rec.Mat = Mat;
            return true;
        }

        public ShapeSample Sample()
        {
            throw new NotImplementedException();
        }

        public float GetArea()
        {
            throw new NotImplementedException();
        }

        private readonly float _a, _b, _c, _d;
        public AABB BBox { get; set; }
        public Material Mat { get; set; }
    }
}
